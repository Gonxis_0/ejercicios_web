import React, { Component } from 'react';
import Dato from './Dato';
import Boton from './Boton';

class Contador extends Component {

    constructor(props) {
        super(props);

        this.state = {
            display: this.props.valorInicial*1
        }

        this.displayMas = this.displayMas.bind(this);
        this.displayMenos = this.displayMenos.bind(this);
    }

    displayMenos() {
        // No hacer esto: this.state.display--;
        if (this.state.display > 0) {
            this.setState({
                display: this.state.display-1
            });
        }
    }

    displayMas() {
        // No hacer esto: this.state.display++;
        if (this.state.display < 10) {
            this.setState({
                display: this.state.display+1
            });
        }
    }

    render() {
        return ( 
            <React.Fragment>
                <button onClick={this.displayMenos}> Menos </button>  
                <Boton clicado={this.displayMenos} texto="Menos" />
                <h2 > { this.state.display } </h2>
                <Dato valor={this.state.display}/> 
                <Boton clicado={this.displayMenos} texto="Mas" />
                <button onClick={this.displayMas}> Mas </button>  
            </React.Fragment >
        );
    }
}

export default Contador;