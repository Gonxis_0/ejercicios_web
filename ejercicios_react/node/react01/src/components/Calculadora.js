import React from 'react';
import './css/Calculadora.css';

export default class Calculadora extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            display : "",
            valorAnterior: 0,
            valorSiguiente: 0,
            operacionAnterior: ""
        }

        this.pulsar = this.pulsar.bind(this);
    }

    pulsar (caracter) {
        let nuevoState = this.state.display + caracter;
        let valorAnterior = this.state.valorAnterior;
        let operacionAnterior = this.state.operacionAnterior;

        switch (caracter) {
            case "C":
                nuevoState = "";
                valorAnterior = 0;
                this.state.valorSiguiente = 0;
                this.state.operacionAnterior = "";
                break;
            case "*":
                this.state.valorAnterior = nuevoState;
                this.state.operacionAnterior = "*";
                nuevoState = "";
                break;
            case "/":
                this.state.valorAnterior = nuevoState;
                this.state.operacionAnterior = "/";
                nuevoState = "";
                break;
            case "+":
                this.state.valorAnterior = nuevoState;
                this.state.operacionAnterior = "+";
                nuevoState = "";
                break;
            case "-":
                this.state.valorAnterior = nuevoState;
                this.state.operacionAnterior = "-";
                nuevoState = "";
                break;
            case "=":
                switch (operacionAnterior) {
                    case "*":
                        nuevoState = parseInt(valorAnterior) * parseInt(nuevoState);
                        nuevoState = "" + nuevoState;
                        break;
                    case "+":
                        nuevoState = parseInt(valorAnterior) + parseInt(nuevoState);
                        nuevoState = "" + nuevoState;
                        break;
                    case "-":
                        nuevoState = parseInt(valorAnterior) - parseInt(nuevoState);
                        nuevoState = "" + nuevoState;
                        break;
                    case "/":
                        nuevoState = parseInt(valorAnterior) / parseInt(nuevoState);
                        nuevoState = "" + nuevoState;
                        break;
                    case "=":
                        console.log("En esta condición entro!");
                        this.state.valorSiguiente = this.state.nuevoState;
                        if (operacionAnterior === '+'){
                            nuevoState = parseInt(nuevoState) + parseInt(this.state.valorSiguiente);
                        } else if (operacionAnterior === '-') {
                            nuevoState = parseInt(nuevoState) - parseInt(this.state.valorSiguiente);
                        } else if (operacionAnterior === '*') {
                            nuevoState = parseInt(nuevoState) * parseInt(this.state.valorSiguiente);
                        } else if (operacionAnterior === '/') {
                            nuevoState = parseInt(nuevoState) / parseInt(this.state.valorSiguiente);
                        }
                        break;
                    default:
                        //Do anything else
                        console.log("Estoy en el default del igual");
                        break;
                }
                break;
            default:
                //Do anything else
                console.log("Estoy en el default general");
                break;
        }

        this.setState({
            display: nuevoState
        });
    }

    render() {
        return (
            <React.Fragment>
                <h3 className="display">{this.state.display}</h3>
                <div>
                    <button className="button" onClick={() => this.pulsar(1)}>1</button>
                    <button className="button" onClick={() => this.pulsar(2)}>2</button>
                    <button className="button" onClick={() => this.pulsar(3)}>3</button>
                    <button className="button" onClick={() => this.pulsar('*')}>*</button>
                </div>
                <div>
                    <button className="button" onClick={() => this.pulsar(4)}>4</button>
                    <button className="button" onClick={() => this.pulsar(5)}>5</button>
                    <button className="button" onClick={() => this.pulsar(6)}>6</button>
                    <button className="button" onClick={() => this.pulsar('/')}>/</button>
                </div>
                <div>
                    <button className="button" onClick={() => this.pulsar(7)}>7</button>
                    <button className="button" onClick={() => this.pulsar(8)}>8</button>
                    <button className="button" onClick={() => this.pulsar(9)}>9</button>
                    <button className="button" onClick={() => this.pulsar('-')}>-</button>
                </div>
                <div>
                    <button className="button" onClick={() => this.pulsar(0)}>0</button>
                    <button className="button" onClick={() => this.pulsar('C')}>C</button>
                    <button className="button" onClick={() => this.pulsar('=')}>=</button>
                    <button className="button" onClick={() => this.pulsar('+')}>+</button>
                </div>
            </React.Fragment>
        );
    }
}