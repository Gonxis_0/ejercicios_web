import React from 'react';
//import './css/Boton.css';

export default class Boton extends React.Component {
    constructor(props) {
        super(props);
    }

    render (){
        return (
            <button onClick={this.props.clicado}> {this.props.texto} </button>
        );
    }
}