import React from 'react';

const Dato = (props) => {
    return <div className="dato">{props.valor}</div>;
}

export default Dato;