import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Contador from './components/Contador';
import Calculadora from './components/Calculadora';

class App extends Component {
    render() {
        return ( 
            <React.Fragment >
                <Calculadora />
            </React.Fragment>
        );
    }
}

export default App;