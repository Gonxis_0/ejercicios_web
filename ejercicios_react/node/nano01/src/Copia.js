
import React from 'react';
import './css/copia'


function transforma(palabra){
    return palabra.split("").reverse().join("");
}


export default class Copia extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            nom: ""
        }
        this.xnom = this.xnom.bind(this);
    }

    xnom(event){
        this.setState({
            nom: event.target.value
        })
    }


    render(){
        return (
            <div className="copia">
            <input onChange={this.xnom} name="nom" value={this.state.nom} />
            <input style={{textAlign: "right"}} value={transforma(this.state.nom)} disabled />
            </div>
        );
    }

}