import React from 'react';
import './css/titulo.less';

const Titulo = (props) => <h1 className="titulo">{props.texto}</h1>;
export default Titulo;


/*
FORMAS EQUIVALENTES:
>>>>>>>>>>>>>>>>>>>>>>

export default (props) => <h1 className="titulo">{props.texto}</h1>;

>>>>>>>>>>>>>>>>>>>>>

function Titulo(props) {
    return <h1 className="titulo">{props.texto}</h1>
}
export default Titulo;

>>>>>>>>>>>>>>>>>>>>>>

class Titulo extends React.Component{
    render(){
        return (
            <h1 className="titulo">{this.props.texto}</h1>
        )
    }
}
export default Titulo;


*/