
import React from 'react';


export default () => <img style={{borderRadius: "100px"}} src="http://lorempixel.com/100/100" />;

/*
equivalente a:

const Logo = () => <img style={{borderRadius: "100px"}} src="http://lorempixel.com/100/100" />;
export default Logo;

o:

function Logo(){
    return <img style={{borderRadius: "100px"}} src="http://lorempixel.com/100/100" />;
}
export default Logo;

*/

