import React from 'react';

import imgOn from "./img/on.jpg";
import imgOff from "./img/off.jpg";

const Bombilla = (props) => {
    let imagen = (props.estado) ? imgOn : imgOff;
    return <img src={imagen} />
}

export default Bombilla;