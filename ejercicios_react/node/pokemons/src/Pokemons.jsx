import React from "react";

import { Table } from 'reactstrap';
import { Link } from "react-router-dom";

export default class Pokemons extends React.Component {

    render() {

        //para crear las filas hacemos un "map" de los pokemons recibidos
        //previamente los ordenamos por id...
        let filasPokemons = this.props.pokemons.sort((a, b) => a.id - b.id).map(pokemon => {
            return (
                <tr key={pokemon.id}>
                    <td>{pokemon.id}</td>
                    <td>{pokemon.photo}</td>
                    <td>{pokemon.name}</td>
                    <td>{pokemon.type}</td>
                    <td><Link to={"/" + pokemon.id}>Detalle</Link></td>
                </tr>
            );
        })

        return(
            <>
            <h2>Pokemons</h2>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th></th>
                            <th>Name</th>
                            <th>Type</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filasPokemons}
                    </tbody>
                </Table>
            </>
        );
    }
}