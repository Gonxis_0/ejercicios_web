
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button } from 'reactstrap';


class Edita extends Component {
    constructor(props) {
        super(props);

        //si no rebem paràmetre considerem item nou
        let id = 0;
        if (this.props.match && this.props.match.params.id) {
            id = this.props.match.params.id * 1;
        }

        this.state = new this.props.model(); //creem objecte buid del tipus del model
        this.state.tornar=false;
    
        this.carregaRegistre = this.carregaRegistre.bind(this);
        this.canviInput = this.canviInput.bind(this);
        this.submit = this.submit.bind(this);

        if (id>0){
            this.carregaRegistre(id);
        }
    }

    //carreguem registre a editar
    carregaRegistre(id) {
        this.props.model.getById(id)
            .then(data => this.setState(data))
            .catch(err => console.log(err));
    }

    //sincronitzem canvi a input
    canviInput(event) {
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    /* goBack() {
        return <Redirect to={`/${this.props.model.getModelName()}`} />
    } */

    // submit
    submit(e) {
        //evitem comportament "normal" del formulari
        e.preventDefault();
        //creem objecte model amb les dades de state
        //el constructor només prendra les que corresponguin als fields
        let ob = new this.props.model(this.state);
        //desem i actualitzem state si ok
        ob.save()
            .then(() => this.setState({ volver: true }))
            .catch(err => {
                console.log(err);
                this.setState({ volver: true });
            });
    }


    render() {

        if (this.state.volver === true) {
            return <Redirect to={'/' + this.props.model.getCollection()} />
        }

        let nombrePokemonMinusculas = this.state.nombre.toLowerCase();

        let image = `https://img.pokemondb.net/artwork/large/${nombrePokemonMinusculas}.jpg`

        return (
            <>
                <h1 className="titol-vista">{this.props.model.getModelName() + ' ' + this.state.nombre}</h1>
                <img src={image} alt={this.state.nombre} width="500px" height="500px" />
                <br />
                <Button disabled color="primary">Atrás</Button>
                {/* <Button onClick={() => this.goBack()}>Atrás</Button> */}
            </>
        );
    }
}






export default Edita;
