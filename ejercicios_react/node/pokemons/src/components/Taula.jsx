import React from "react";

import { Button, Table } from 'reactstrap';
import DataModal from './DataModal';
import { Redirect } from 'react-router-dom';

import Pagination from './Paginationss';

export default class Lista extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            data: [],
            itemToDelete: 0,
            modalAction: null,
            modalVisible: false,
            modalTitle: '',
            modalBody: '',
            modalBtnOk: '',
            modalBtnNok: '',
            gotoNou: false,
            gotoEdit: 0,
            loadedModel: '',
            currentPage: 1,
            currentPokemon: 100,
            totalPages: 9
        }

        this.edit = this.edit.bind(this);
        this.nouRegistre = this.nouRegistre.bind(this);
        this.delete = this.delete.bind(this);
        this.executeDelete = this.executeDelete.bind(this);
        this.getData = this.getData.bind(this);
        // this.getData();
    }


    //nou registre
    nouRegistre() {
        this.setState({ gotoNou: true });
    }


    //obté dades del model rebut via props
    getData() {
        this.props.model.getAll(this.state.currentPage, this.state.currentPokemon)
            .then(data => this.setState({ data }))
            .then(() => this.setState({ loadedModel: this.props.model.getModelName() }))
            .catch(err => console.log(err));
    }

    //configura i mostra modal de confirmació per editar
    edit(id) {
        this.setState({ gotoEdit: id });
    }


    //configura i mostra modal de confirmació per eliminar
    delete(id) {
        this.setState({
            modalVisible: true,
            modalAction: this.executeDelete,
            modalParameter: id,
            modalTitle: 'Confirmació',
            modalBody: 'Eliminar registre?',
            modalBtnOk: 'Eliminar',
            modalBtnNok: 'Cancel.lar'
        });
    }

    executeDelete(resp, id) {
        if (resp === 'ok') {
            let novaData = this.state.data.filter(el => el.id !== id);
            this.setState({ data: novaData });
            this.props.model.deleteById(id);
        }
        this.setState({ modalVisible: false });
    }

    
    onPageChanged = data => {
        const { allPokemons } = this.state.data;
        const { currentPage, totalPages, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentPokemon = this.state.data.slice(offset, offset + pageLimit);

        this.setState({ currentPage, currentPokemon, totalPages });
    }
    

    render() {

        /** */
        const totalPokemons = this.state.data.length;

        //if (totalPokemons === 0) return null;
        /**/
        // com que utilitzem un mateix component, cal verificar si dades carregades 
        // son les del model solicitat
        // la primera vegada sempre serà així
        if (this.state.loadedModel !== this.props.model.getModelName()) {
            this.getData();
            return <></>;
        }

        //anem a nou registre si cal
        if (this.state.gotoNou) {
            return <Redirect to={`/${this.props.model.getCollection()}/nuevo`} />
        }

        //anem a editar registre si cal
        if (this.state.gotoEdit) {
            return <Redirect to={`/${this.props.model.getModelName()}/${this.state.gotoEdit}`} />
            // return <Redirect to={`/${this.props.model.getCollection()}/detalle/${this.state.gotoEdit}`} />  
        }

        let fieldNames = this.props.model.getFields();
        // titols de les columnes a partir dels camps del model
        let titolsColumnes = fieldNames.map(el => <th key={el.name}>{el.name}</th>);

        /**
         *  Pruebas para la imagen
         */
        /* let imagenes = this.state.data.sort((a, b) => a.id - b.id)
            .map(item => {
                console.log(item)
                let srcImg = `${url}${item.nombre.toLowerCase()}.png`
                let image = <td key={item.nombre[item.id]}><img src={srcImg} alt={item.nombre} /></td>
                return image;
            }) */
        /**
         * Fin de pruebas para imagen
         */

        let url = `https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/`;

        //creem les files a partir de dades rebudes, ordenades per id
        let filas = this.state.data.sort((a, b) => a.id - b.id)
            .map(item => {
                let srcImg = `${url}${item.nombre.toLowerCase()}.png`
                let image = <td ><img src={srcImg} alt={item.nombre} /></td>
                let cols = fieldNames
                    .map(el => <td key={el.name}>{item[el.name]}</td>)
                //let image = `https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal/${nombrePokemonMinusculas}.png`
                //let images = fieldNames.map(el => <td key={el.name}><img src={image} alt={item[el.name]} /></td>)
                return (
                    <tr key={item.id}>
                        <td >{item.id}</td>
                        {/* <td ><img src={srcImg} alt="nombre" /></td> */}
                        {/* {imagenes} */}
                        {image}
                        {cols}
                        {/* <td><i onClick={() => this.edit(item.id)} className="fa fa-edit clicable blue-icon" />Detalle</td> */}
                        <td><Button onClick={() => this.edit(item.id)} color="primary">Detalle</Button></td>
                        {/* <td><i onClick={() => this.detail(item.id)} className="fa fa-detail clicable blue-icon" /></td> */}
                    </tr>
                );
            })


        return (
            <>
                <h1 className="titol-vista">{this.props.model.getTitle()}</h1>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th></th>
                            {titolsColumnes}
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {/* {imagenes} */}
                        {filas}
                    </tbody>
                </Table>

                {/* <Button onClick={this.nouRegistre} >Nou registre</Button> */}

                <DataModal visible={this.state.modalVisible}
                    title={this.state.modalTitle}
                    body={this.state.modalBody}
                    okText={this.state.modalBtnOk}
                    nokText={this.state.modalBtnNok}
                    action={this.state.modalAction}
                    parameter={this.state.modalParameter}
                />
                {/** */
                <Pagination
                    totalRecords={totalPokemons}
                    pageLimit={100}
                    pageNeighbours={1}
                    onPageChanged={this.onPageChanged}
                />
                /**/}
            </>
        );
    }
}