import React from 'react';
import {  NavLink } from "react-router-dom";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem
} from 'reactstrap';

export default class NavMenu extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="primary" light expand="md">
          <NavbarBrand href="/"><i className="fa fa-gears" /> <span>Pokemon</span></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink className="link nav-link" to="/pokemons">Listado</NavLink>
              </NavItem>
           
              {/* <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Datos
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <NavItem>
                      <NavLink className="link nav-link" to="/pokemons">Pokemons</NavLink>
                    </NavItem>
                </DropdownItem> */}
                  {/* <DropdownItem divider />
                  <DropdownItem>
                  <NavItem>
                      <NavLink className="link nav-link" to="/tasques">Tasques</NavLink>
                    </NavItem>
                  </DropdownItem> */}
                {/* </DropdownMenu>
              </UncontrolledDropdown> */}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}