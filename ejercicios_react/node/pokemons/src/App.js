import React from "react";
import { BrowserRouter, Link, NavLink, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

// importem components
import Inici from './components/Inici';
import Taula from './components/Taula';
import NavMenu from './components/NavMenu';
import Edita from './components/Edita';
import P404 from './components/P404';

//importem models
import Pokemon from './models/Pokemon';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './app.css';


//xmysql -h localhost -u root -p admin -d codesplai

// clase App 
export default class App extends React.Component {

  constructor(props) {
    super(props);


    this.state = {
      contactos: [],
      ultimoContacto: 3
    };

  }




  render() {

    if (this.state.contactos.length !== 0){
      return <h1>Carregant dades...</h1>;
    }
    

    return (
      <BrowserRouter>

      <NavMenu />
        <Container>
          
            <Row>
            <Col>
              <Switch>
                <Route exact path="/" component={Inici} />
            
                <Route exact path="/pokemons" render={() => <Taula model={Pokemon} />} />
                <Route path="/pokemon/:id" 
                    render={(props) => <Edita {...props} model={Pokemon} />} /> 

                <Route component={P404} />
              </Switch>
            </Col>
          </Row>

        </Container>
      </BrowserRouter>
    );

  }

}
