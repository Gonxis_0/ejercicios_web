use vgames;
SELECT name, platform, year, global_sales from videogames order by global_sales desc limit 10;
SELECT name FROM videogames order by name desc limit 10;
SELECT COUNT(distinct platform) as totalPlatforms from videogames;

SELECT COUNT(distinct name) as totalGames from videogames where Platform like '%ps%' or platform like 'play%';

#Número de juegos publicados del género "sports" hasta el año 2000 incluido
SELECT COUNT(name) as totalGamesUntil2000 from videogames where genre = 'sports' and Year <= 2000;

/*El juego con más unidades vendidas en un solo año (ventas globales) para el género "sports"
en todos los tiempos, y el año en que fue.*/
SELECT name, year from videogames where genre = 'sports' order by Global_Sales desc limit 1;

/*Los top 10 juegos con mayores ventas anuales de todos los tiempos. Mostrar juego, año y
ventas globales en orden descendiente.*/
SELECT name, year, Global_Sales from videogames order by Global_Sales desc limit 10;

/*Los juegos que han conseguido vender más de 25M de unidades anuales, mostrar nombre,
año y volumen de ventas, ordenados por año.*/
SELECT name, year, Global_Sales from videogames where Global_Sales > 25 order by year;

/*Suma de juegos vendidos en total para la plataforma “PSP” (cantidad).*/
SELECT SUM(name) as totalSaleGames from videogames where platform = 'PSP';

/*Suma de juegos vendidos en europa y norte américa para la plataforma "wii".*/
SELECT SUM(NA_Sales + EU_Sales) as totalSaleGamesEUUA from videogames where Platform = 'wii';

/*Suma de juegos de playstation vendidos en total en europa, norte américa y japón (por
separado)*/
SELECT ROUND(SUM(EU_Sales),2) as EUSales, ROUND(SUM(NA_Sales),2) as NASales, ROUND(SUM(JP_Sales),2) as JPSales from videogames where Platform like 'ps%';

/*Suponiendo la población actual, el "continente" con mayor índice de ventas por cápita
○ EEUU 325.7 M
○ EUROPA 741.3 M
○ JAPAN 126.8 M
*/
SELECT sum(eu_Sales)/741.3 as europa, sum(na_sales)/325.7 as nothAmerica, sum(jp_sales)/126.8 as japan from videogames;

/*La suma de ventas en cada país/continente (eu, na, jp), solo para los géneros: racing,
sports y simulation (los 3 sumados en una sola columna)*/
SELECT SUM(NA_Sales + EU_Sales + JP_Sales) as salesEuNaJp 
from videogames 
where genre = 'racing' 
	or genre = 'sports' 
	or genre = 'simulation';

/*La suma de ventas en cada continente, solo para los géneros: racing, sports y
simulation SEPARANDO LOS 3 GÉNEROS en 3 columnas (group by)*/
SELECT genre, SUM(NA_Sales + EU_Sales + JP_Sales) as salesEuNaJp 
from videogames 
where genre = 'racing' 
	or genre = 'sports' 
	or genre = 'simulation'
group by genre;

/*Suma total de ventas por plataforma en los años 2008-2010*/
SELECT platform, SUM(global_sales) as ventasTotales08To10 from videogames where Year >= 2008 and Year <= 2010 group by Platform;

/*● Suma de ventas por año para el género “Sports”*/
SELECT year, SUM(global_sales) as salesYearSports from videogames where genre = 'sports' group by year;

/*● Suma de ventas por año para el género “Strategy”*/
SELECT year, SUM(global_sales) as salesYearSports from videogames where genre = 'strategy' group by year;

/*Los 10 juegos más vendidos de la historia, en volumen de ventas totales (eu+na+jp)*/
SELECT name, SUM(EU_Sales + NA_Sales + JP_Sales) as totalSales from videogames group by name order by totalSales desc limit 10;

/*● El año en que se vendieron más videojuegos de estrategia*/
#SELECT year, max(global_sales) as numberStrategy from videogames where genre = 'strategy';
SELECT year, sum(Global_Sales) as total from videogames where genre = 'strategy' group by year order by total desc limit 1;

/*
Los 10 juegos más vendidos de la historia, en volumen de ventas totales (eu+na+jp)
○ columnas: titulo, nombre de género
*/
SELECT name, genre, SUM(EU_Sales + NA_Sales + JP_Sales) as totalSales from videogames group by name order by totalSales desc limit 10;

/*
● Mostrar las 5 plataformas con mayor número de ventas en período 2010-2015
○ columnas: nombre de plataforma, suma de ventas total (eu+na+jp)
*/
SELECT platform, SUM(EU_Sales + NA_Sales + JP_Sales) as totalSales from videogames where year between 2010 and 2015 group by platform order by totalSales desc limit 5;

/*
● Mostrar los 5 publishers con mayor número de ventas en período 2010-2015
○ columnas: nombre de publisher, suma de ventas total (eu+na+jp)
*/
SELECT Publisher, SUM(EU_Sales + NA_Sales + JP_Sales) as totalSales from videogames where year >= 2010 and year <= 2015 group by Publisher order by totalSales desc limit 5;

/*
Con having (y todo lo demás)
● Mostrar evolución de ventas para las playstation en todos los años
○ columnas: año, nombre de plataforma, suma de ventas total (eu+na+jp)
○ orden ascendente por año
○ plataformas: PS, PS2, PS3, PS4
*/
SELECT group_concat(distinct(platform)), SUM(EU_Sales + NA_Sales + JP_Sales) as totalSales 
from videogames 
where Platform in ('PS', 'PS2', 'PS3', 'PS4') 
group by year
order by platform, year;

/*
● El año en que se vendieron más juegos de "pokemon" (en todas sus variantes)
*/
SELECT year, max(global_sales) as globalSalePokemon 
from videogames 
where name like '%pokemon%' 
group by year 
order by globalSalePokemon desc 
limit 1;

/*
● La saga más vendida de la historia...
*/
UPDATE videogames set saga = 'mario' where name like '%mario%';
UPDATE videogames set saga = 'pokemon' where name like '%pokemon%';
UPDATE videogames set saga = 'The Legend of Zelda' where name like '%zelda%';

select name from videogames where name like '%pokemon%';
select COUNT(name) from videogames where name like '%pokemon%';
SELECT COUNT(saga) from videogames where saga = 'pokemon';
SELECT name from videogames;