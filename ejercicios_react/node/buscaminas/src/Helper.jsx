import React from 'react';


export const MOSTRAR_BOMBAS = false;
export const BOMB_ICON = (MOSTRAR_BOMBAS) ? <i className="fas fa-bomb"></i> : <i></i>;
export const FILAS = 10;
export const COLUMNAS = 15;
export const BOMBAS = 20;

//devuelve una posición random en forma de hash 

export let getRandomHash = function (maxX, maxY) {
    return getHash(
        getRandomIntInclusive(1, maxX),
        getRandomIntInclusive(1, maxY)
    )
}

//devuelve hash de x,y ej 2,3 >> 2*1000+3 = 2003 
export let getHash = function (x, y) { return x * 1000 + y; }

//devuelve número random entre min y max, ambos incluidos
export let getRandomIntInclusive = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
// devuelve la x (columna) de un hash ej 2003 >> 2
export function getHashX(hash) {
    return Math.floor(hash / 1000);
}
// devuelve la y (fila) de un hash ej 2003 >> 3
export function getHashY(hash) {
    return hash % 1000;
}
