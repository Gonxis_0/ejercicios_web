import React from "react";
import Tablero from './Tablero';

import {FILAS, COLUMNAS, BOMBAS, getRandomHash, getHash} from './Helper';

class Buscaminas extends React.Component {

    constructor(props) {
        super(props);

        this.saveData = this.saveData.bind(this);
        this.loadData = this.loadData.bind(this);
        this.getRandomBombs = this.getRandomBombs.bind(this);
        this.clicado = this.clicado.bind(this);
        //en state guardamos unicamente un objeto positions creado a partir de los parámetros indicados
        //positions contiene cada hash de posición asociado a los datos de la celda
        this.state = {
            positions: this.createPositions(FILAS, COLUMNAS, BOMBAS)
        };
    }

    //método que crea las posiciónes
    createPositions(ymax, xmax, numbombs) {
        //obtenemos bombas random, array de hashes
        let bombs = this.getRandomBombs(ymax, xmax, numbombs);
        let positions = {}; //creamos objeto vacio
        //doble bucle filas, columnas
        for (let y = 1; y <= ymax; y++) {
            for (let x = 1; x <= xmax; x++) {
                let hash = getHash(x, y); //obtenemos hash para x,y
                // cada hash va asociado a un objeto {isBomb, isOpen, bombs}
                // si el hash forma parte del array de bombas isBombs será true
                // todos empiezan con isOpen false
                // bombs es el número de bombas vecinas, se calculará al clicar
                positions[hash] = { isBomb: bombs.indexOf(hash) !== -1, isOpen: false, bombs: 0 };
            }
        }
        //guardamos también en positions los datos cols y rows
        positions.cols = xmax;
        positions.rows = ymax;
        return positions;
    }

    //extra guardado de datos/tablero
    saveData() {
        var jsonData = JSON.stringify(this.state);
        localStorage.setItem("tododata", jsonData);
    }

    //carga de tablero
    loadData() {
        var text = localStorage.getItem("tododata");
        if (text) {
            var obj = JSON.parse(text);
            this.setState(obj);
        }
    }

    //devuelve array de hashes de posiciones aleatorias 
    getRandomBombs(numrows, numcols, numBombs) {
        let i = 0;
        let bombs = [];
        while (i < numBombs) {
            let hash = getRandomHash(numcols, numrows);
            if (bombs.indexOf(hash) === -1) {
                bombs.push(hash);
                i++;
            }
        }
        return bombs;
    }

    //clicado recibe el hash de la posición que ha recibido un click
    clicado(hash) {
        // si se trata de una posición "abierta" no hacemos nada
        if (this.state.positions[hash].isOpen) return;
        //creamos nuevo array con posiciones del state.positions
        //puesto que éste no debe modificarse directamente
        let positions = JSON.parse(JSON.stringify(this.state.positions))
        //analizamos las consecuencias del clicado en "hash" en base a "positions"
        this.analiza(positions, hash);
        //finalmente, tras el análisis y los cambios necesarios en positions reasignamos state
        this.setState({ positions: positions });
    }

    //analizamos consecuencias de clicado en hash sobre mapa positions
    analiza(positions, hash) {
        //si la posición esta abierta, volvemos sin cambios
        if (positions[hash].isOpen) return positions;

        positions[hash].isOpen = true; // abrimos la posición

        let bombsNear = this.cuentaBombas(hash);// contamos bombas cercanas
        positions[hash].bombs = bombsNear; // asignamos numero de bombas

        //si se detecta que no hay bombas alrededor de la posición analizada
        //se "clican" las posiciones vecinas para "destapar" automáticamente
        //las que tampoco tengan bombas cerca
        if (bombsNear == 0) {
            /////////////////////
            /////////////////////
            /////////////////////
            /////////////////////
            /////////////////////
        }
    }


    //cuenta el número de bombas que rodean a una posición
    cuentaBombas(hash) {
        let validAroundHashes = this.validAround(hash);
        let bombs = 0;
        for (let i in validAroundHashes) {
            //console.log(validAroundHashes[i]);
            if (this.state.positions[validAroundHashes[i]].isBomb) bombs++;
        }
        return bombs;
    }

    //devuelve lista de "hashes" de posiciones vecinas a "hash" (válidas)
    validAround(hash) {
        let hashes = [];
        /////////////////////////////////////
        /////////////////////////////////////
        /////////////////////////////////////
        /////////////////////////////////////
        /////////////////////////////////////
        /////////////////////////////////////
        /////////////////////////////////////
        /////////////////////////////////////
        return hashes;

    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <Tablero onClick={this.clicado} positions={this.state.positions} />
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-4">
                        <button className="btn btn-success btn-margin" onClick={this.saveData}>Desar</button>
                        <button className="btn btn-warning" onClick={this.loadData}>Recuperar</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Buscaminas;