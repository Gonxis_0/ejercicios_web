import React from "react";
import { BOMB_ICON } from './Helper';

class Celda extends React.Component {
    render() {

        let clase = "cssCell celda ";
        if (!this.props.cell.isOpen) {
            clase += "oculta";
        } else if (this.props.cell.isBomb) {
            clase += "bomba";
        }
        //establecemos el contenido de la celda
        //si bombs>0 mostramos el número
        let content = (this.props.cell.bombs > 0) ? this.props.cell.bombs : "";
        //si es una bomba mostramos el icono
        if (this.props.cell.isBomb) content = BOMB_ICON;

        return (
            <div onClick={() => this.props.onClick(this.props.hash)} className={clase}>
                {content}
            </div>
        )
    }
}

export default Celda;