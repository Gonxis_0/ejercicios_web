import React from "react";
import Fila from './Fila';

class Tablero extends React.Component {
    render() {
        let filas = [];
        // para cada fila añadimos un componente <Fila /> al array filas
        for (let row = 1; row <= this.props.positions.rows; row++) {
            filas.push(<Fila onClick={this.props.onClick} positions={this.props.positions} key={"fila" + row} fila={row} />)
        }

        return (
            <div className="cssTable">
                {filas}
            </div>
        )
    }
}

export default Tablero;