import React from "react";
import Celda from './Celda';
import { getHash } from './Helper';

class Fila extends React.Component {
    render() {
        let cells = [];
        //añadmos a cells las celdas de la fila correspondiente
        for (let x = 1; x <= this.props.positions.cols; x++) {
            let hash = getHash(x, this.props.fila);
            let cell = this.props.positions[hash];
            cells.push(<Celda onClick={this.props.onClick} cell={cell} key={hash} hash={hash} />)
        }
        return (<div className="cssRow">{cells}</div>
        );
    }
}

export default Fila;