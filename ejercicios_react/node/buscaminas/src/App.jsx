import React from "react";
import Buscaminas from './Buscaminas';

export default class App extends React.Component {
    render() {
        return (
            <>
                <Buscaminas />
            </>
        );
    }
}

/* export default () => (
  <>
    <h1>Welcome to React Parcel Micro App!</h1>
    <p>Hard to get more minimal than this React app.</p>
  </>
); */
