import React from "react";
import './App.css';
import TodoApp from './TodoApp';

export default () => (
  <>
    <h1>Test Pagination!</h1>
    <TodoApp />
  </>
);
