import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button} from 'reactstrap';


class EliminaAlumno extends Component {
    constructor(props) {
        super(props);

        //leemos datos recibidos

        let id = this.props.match.params.idAlumno * 1;
        let actual = this.props.alumnos.filter(el => el.id===id)[0];

        this.state = {
            name: actual.name,
            email: actual.email,
            yearsOld: actual.yearsOld,
            gender: actual.gender,
            id: actual.id,
            volver: false
        };

        this.eliminar = this.eliminar.bind(this);
        this.volver = this.volver.bind(this);

    }

    eliminar(){
        this.props.eliminaAlumno(this.state.id);
        this.setState({volver: true});
    }


    volver(){
        this.setState({volver: true});
    }

    render() {

        if (this.state.volver === true) {
            return <Redirect to='/alumnos' />
        }

        return (
            <>
                <h3>Desea elminar a {this.state.name}</h3>
                <Button onClick={this.eliminar} color="danger">Sí</Button>
                <Button onClick={this.volver} color="success">No</Button>
            </>

        );
    }
}






export default EliminaAlumno;
