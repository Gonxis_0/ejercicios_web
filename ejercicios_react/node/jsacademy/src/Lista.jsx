import React from "react";

import { Table } from 'reactstrap';
import { Link } from "react-router-dom";

export default class Lista extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {


        //para crear las filas hacemos un "map" de los alumnos recibidos
        //previamente los ordenamos por id...
        let filasAlumnos = this.props.alumnos.sort((a, b) => a.id - b.id).map(alumno => {
            return (
                <tr key={alumno.id}>
                    <td>{alumno.id}</td>
                    <td>{alumno.name}</td>
                    <td>{alumno.email}</td>
                    <td>{alumno.yearsOld}</td>
                    <td>{alumno.gender}</td>
                    <td><Link to={"/modificaAlumno/" + alumno.id}>Editar</Link></td>
                    <td><Link to={"/eliminaAlumno/" + alumno.id}>Eliminar</Link></td>
                </tr>
            );
        })

        let filasCursos = this.props.cursos.sort((a, b) => a.id - b.id).map(curso => {
            return (
                <tr key={curso.id}>
                    <td>{curso.id}</td>
                    <td>{curso.name}</td>
                    <td>{curso.speciality}</td>
                    <td><Link to={"/modificaCurso/" + curso.id}>Editar</Link></td>
                    <td><Link to={"/eliminaCurso/" + curso.id}>Eliminar</Link></td>
                </tr>
            );
        })


        return (
            <>
            <hr></hr>
                <hr></hr>
                <h2>Alumnos</h2>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Years old</th>
                            <th>Gender</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filasAlumnos}
                    </tbody>
                </Table>
                <hr></hr>
                <hr></hr>
                <br></br>
                <hr></hr>
                <hr></hr>
                <h2>Cursos</h2>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Speciality</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filasCursos}
                    </tbody>
                </Table>
                <hr></hr>
                <hr></hr>
            </>
        );
    }


}

