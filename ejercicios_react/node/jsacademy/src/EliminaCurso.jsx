import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button} from 'reactstrap';


class EliminaCurso extends Component {
    constructor(props) {
        super(props);

        //leemos datos recibidos

        let id = this.props.match.params.idCurso * 1;
        let actual = this.props.cursos.filter(el => el.id===id)[0];

        this.state = {
            name: actual.name,
            speciality: actual.speciality,
            id: actual.id,
            volver: false
        };

        this.eliminar = this.eliminar.bind(this);
        this.volver = this.volver.bind(this);

    }

    eliminar(){
        this.props.eliminaCurso(this.state.id);
        this.setState({volver: true});
    }


    volver(){
        this.setState({volver: true});
    }

    render() {

        if (this.state.volver === true) {
            return <Redirect to='/cursos' />
        }

        return (
            <>
                <h3>Desea elminar a {this.state.name}</h3>
                <Button onClick={this.eliminar} color="danger">Sí</Button>
                <Button onClick={this.volver} color="success">No</Button>
            </>

        );
    }
}






export default EliminaCurso;