import React from "react";
import { BrowserRouter, Switch, Route, NavLink } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';

// importamos los componentes de la aplicación (vistas)
import Home from './Home';
import NuevoAlumno from './NuevoAlumno';
import NuevoCurso from './NuevoCurso';
import ModificaAlumno from './ModificaAlumno';
import ModificaCurso from './ModificaCurso';
import EliminaAlumno from './EliminaAlumno';
import EliminaCurso from './EliminaCurso';
import Alumnos from './Alumnos';
import Cursos from './Cursos';
import P404 from './P404';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
//import 'font-awesome/css/font-awesome.min.css';
import './App.css';

// importamos modelos
import Alumno from './Alumno';
import Curso from './Curso';

//import BootstrapSelect from './BootstrapSelect';

// importamos filepond
import 'filepond/dist/filepond.min.css';
//import { FilePond } from 'react-filepond';
import { registerPlugin } from 'react-filepond';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
registerPlugin(FilePondPluginImagePreview);


// clase App 
export default class App extends React.Component {

    constructor(props) {
        super(props);

        const alumnosInicio = [
            new Alumno(1, "Tomb", "tomb@riddle.com", 50, "Female"),
            new Alumno(2, "007", "james@bond.com", 65, "Male"),
            new Alumno(3, "spiderman", "peter@parker.com", 99, "Male"),
            new Alumno(4, "Pipi", "calzaslargas@pipi.com", 17, "Femal")
        ];

        const cursosInicio = [
            new Curso(1, "Prácticas de moto", "Conducción"),
            new Curso(2, "Curso de JS", "Informática"),
            new Curso(3, "Curso de primeros auxilios", "Enfermería")
        ];

        this.state = {
            alumnos: alumnosInicio,
            ultimoAlumno: 4,
            cursos: cursosInicio,
            ultimoCurso: 3
        }

        this.guardaAlumno = this.guardaAlumno.bind(this);
        this.guardaCurso = this.guardaCurso.bind(this);
        this.eliminaAlumno = this.eliminaAlumno.bind(this);
        this.eliminaCurso = this.eliminaCurso.bind(this);

        //this.toggle = this.toggle.bind(this);
    }

    componentDidMount() {
    this.hydrateStateWithLocalStorage();

    // add event listener to save state to localStorage
    // when user leaves/refreshes the page
    window.addEventListener(
      "beforeunload",
      this.saveStateToLocalStorage.bind(this)
    );
  }

  componentWillUnmount() {
    window.removeEventListener(
      "beforeunload",
      this.saveStateToLocalStorage.bind(this)
    );

    // saves if component has a chance to unmount
    this.saveStateToLocalStorage();
  }

  hydrateStateWithLocalStorage() {
    // for all items in state
    for (let key in this.state) {
      // if the key exists in localStorage
      if (localStorage.hasOwnProperty(key)) {
        // get the key's value from localStorage
        let value = localStorage.getItem(key);

        // parse the localStorage string and setState
        try {
          value = JSON.parse(value);
          this.setState({ [key]: value });
        } catch (e) {
          // handle empty string
          this.setState({ [key]: value });
        }
      }
    }
  }

  saveStateToLocalStorage() {
    // for every item in React state
    for (let key in this.state) {
      // save to localStorage
      localStorage.setItem(key, JSON.stringify(this.state[key]));
    }
  }

    guardaAlumno(datos) {
        //solo si id=0 asignamos nuevo id y actualizamos ultimoContacto
        if (datos.id === 0) {
            datos.id = this.state.ultimoAlumno + 1;
            this.setState({ ultimoAlumno: datos.id });
        }
        // comprobaciones adicionales... email ya existe? datos llenos?
        // si todo ok creamos Alumno y lo añadimos a la lista
        let nuevo = new Alumno(datos.id, datos.name, datos.email, datos.yearsOld, datos.gender);
        // si alumno existe lo eliminamos!
        // esto es porque podemos llegar aquí desde nuevo alumno o desde modifica alumno
        let nuevaLista = this.state.alumnos.filter(el => el.id !== nuevo.id);
        //añadimos elemento recien creado
        nuevaLista.push(nuevo);
        // finalmente actualizamos state
        this.setState({ alumnos: nuevaLista });

        //localStorage.setItem('alumnos', JSON.stringify(nuevaLista));

    }

    guardaCurso(datos) {
        //solo si id=0 asignamos nuevo id y actualizamos ultimoContacto
        if (datos.id === 0) {
            datos.id = this.state.ultimoCurso + 1;
            this.setState({ ultimoCurso: datos.id });
        }
        // comprobaciones adicionales... email ya existe? datos llenos?
        // si todo ok creamos Curso y lo añadimos a la lista
        let nuevo = new Curso(datos.id, datos.name, datos.speciality);
        // si curso existe lo eliminamos!
        // esto es porque podemos llegar aquí desde nuevo curso o desde modifica curso
        let nuevaLista = this.state.cursos.filter(el => el.id !== nuevo.id);
        //añadimos elemento recien creado
        nuevaLista.push(nuevo);
        // finalmente actualizamos state
        this.setState({ cursos: nuevaLista });

        //localStorage.setItem('cursos', JSON.stringify(nuevaLista));
    }


    eliminaAlumno(idEliminar) {
        //creamos lista a partir de state.alumnos, sin el alumno con el id recibido
        let nuevaLista = this.state.alumnos.filter(el => el.id !== idEliminar);
        //asignamos a alumnos
        this.setState({ alumnos: nuevaLista });

        //localStorage.setItem('alumnos', JSON.stringify(nuevaLista));        
    }

    eliminaCurso(idEliminar) {
        //creamos lista a partir de state.cursos, sin el curso con el id recibido
        let nuevaLista = this.state.cursos.filter(el => el.id !== idEliminar);
        //asignamos a alumnos
        this.setState({ cursos: nuevaLista });

        //localStorage.setItem('cursos', JSON.stringify(nuevaLista));
    }

    // guardado de datos
    saveData(){
        var jsonData = JSON.stringify(this.state);
        localStorage.setItem("js_academy", jsonData);
    }
         
    //carga de datos
    loadData(){
        var text = localStorage.getItem("js_academy");
        if (text){
            var obj = JSON.parse(text);
            this.setState(obj);
        }
    }


    render() {
        return (
            <BrowserRouter>
                <Container>
                    <h1>JS-Academy</h1>
                    <Row>
                        <Col>
                            <ul className="App-list-unstyled App-menu">
                                <li> <NavLink className="link" to="/home">Home</NavLink> </li>
                                <li> <NavLink className="link" to="/cursos">Cursos</NavLink> </li>
                                <li> <NavLink className="link" to="/alumnos">Alumnos</NavLink> </li>
                                <li> <NavLink className="link" to="/nuevoAlumno">Nuevo Alumno</NavLink> </li>
                                <li> <NavLink className="link" to="/nuevoCurso">Nuevo Curso</NavLink> </li>
                            </ul>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Switch>
                                <Route exact path="/" component={Home} />
                                <Route path="/home" component={Home} />
                                <Route path="/cursos" render={() => <Cursos cursos={this.state.cursos} />} />
                                <Route path="/alumnos" render={() => <Alumnos alumnos={this.state.alumnos} />} />
                                <Route path="/nuevoAlumno" render={() => <NuevoAlumno guardaAlumno={this.guardaAlumno} />} />
                                <Route path="/nuevoCurso" render={() => <NuevoCurso guardaCurso={this.guardaCurso} />} />
                                <Route path="/modificaAlumno/:idAlumno" render={(props) => <ModificaAlumno alumnos={this.state.alumnos} guardaAlumno={this.guardaAlumno} {...props} />} />
                                <Route path="/modificaCurso/:idCurso" render={(props) => <ModificaCurso cursos={this.state.cursos} guardaCurso={this.guardaCurso} {...props} />} />
                                <Route path="/eliminaAlumno/:idAlumno" render={(props) => <EliminaAlumno alumnos={this.state.alumnos} eliminaAlumno={this.eliminaAlumno} {...props} />} />
                                <Route path="/eliminaCurso/:idCurso" render={(props) => <EliminaCurso cursos={this.state.cursos} eliminaCurso={this.eliminaCurso} {...props} />} />
                                <Route component={P404} />
                            </Switch>
                        </Col>
                    </Row>
                    {/* <BootstrapSelect /> */}
                    {/* <FilePond allowMultiple={true} /> */}
                </Container>
            </BrowserRouter>
        );

    }

}
