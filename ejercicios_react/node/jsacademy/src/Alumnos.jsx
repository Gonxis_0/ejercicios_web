import React from "react";

import { Table } from 'reactstrap';
import { Link } from "react-router-dom";

export default class Alumnos extends React.Component {

    render() {

        //para crear las filas hacemos un "map" de los alumnos recibidos
        //previamente los ordenamos por id...
        let filasAlumnos = this.props.alumnos.sort((a, b) => a.id - b.id).map(alumno => {
            return (
                <tr key={alumno.id}>
                    <td>{alumno.id}</td>
                    <td>{alumno.name}</td>
                    <td>{alumno.email}</td>
                    <td>{alumno.yearsOld}</td>
                    <td>{alumno.gender}</td>
                    <td><Link to={"/modificaAlumno/" + alumno.id}>Editar</Link></td>
                    <td><Link to={"/eliminaAlumno/" + alumno.id}>Eliminar</Link></td>
                </tr>
            );
        })

        return (
            <>
                <br></br>
                <br></br>
                <hr></hr>
                <hr></hr>
                <h2>Alumnos</h2>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Years old</th>
                            <th>Gender</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filasAlumnos}
                    </tbody>
                </Table>
            </>
        );
    }


}

