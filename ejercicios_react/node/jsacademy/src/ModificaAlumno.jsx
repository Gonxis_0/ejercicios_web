
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

// clase idéntica a NuevoContacto
// excepto que recibimos los datos de un contacto existente
// y la lista completa de contactos
class ModificaAlumno extends Component {
    constructor(props) {
        super(props);

        //recibimos id a modificar
        let id = this.props.match.params.idAlumno * 1;
        //obtenemos el contacto a modificar
        let alumnoModificar = this.props.alumnos.filter(el => el.id===id)[0];
        //establecemos state con los datos del contacto a modificar
        //incluido id
        this.state = {
            name: alumnoModificar.name,
            email: alumnoModificar.email,
            yearsOld: alumnoModificar.yearsOld,
            gender: alumnoModificar.gender,
            id: alumnoModificar.id,
            volver: false
        };

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }

    cambioInput(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });

        //localStorage.setItem(name, value);
    }
    
    // el método submit envia también el id para que guardaContacto sepa qué contacto modificar
    submit(e) {
        this.props.guardaAlumno({
            name: this.state.name,
            email: this.state.email,
            yearsOld: this.state.yearsOld,
            gender: this.state.gender,
            id: this.state.id
        });
        e.preventDefault();
        this.setState({ volver: true });
    }

    render() {

        if (this.state.volver === true) {
            return <Redirect to='/alumnos' />
        }

        return (
            <>
            <br></br>
            <br></br>
            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nameInput">Name</Label>
                            <Input type="text"
                                name="name"
                                id="nameInput"
                                value={this.state.name}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="emailInput">E-mail</Label>
                            <Input type="text" name="email" id="emailInput"
                                value={this.state.email}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup className="formGroup">
                            <Label for="yearsOldInput">Years Old</Label>
                            <Input type="text" name="yearsOld" id="yearsOldInput"
                                value={this.state.yearsOld}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup className="formGroup">
                            <Label for="genderInput">Gender</Label>
                            <select value={this.state.gender}
                                onChange={this.cambioInput} name="gender" id="genderInput">
                                <optgroup label="Gender">
                                    <option key="second" hidden>Select an option</option>
                                    <option key="Female" value="Female">Female</option>
                                    <option key="Male" value="Male">Male</option>
                                </optgroup>
                            </select>
                        </FormGroup>
                    </Col>
                </Row>


                <Row>
                    <Col>
                        <Button color="primary">Guardar</Button>
                    </Col>
                </Row>
            </Form>
            </>

        );
    }
}






export default ModificaAlumno;
