import React from "react";
import image  from "./img/jsAcademy.jpg";

//https://fontawesome.com/v4.7.0/icons/

export default () => (
    <div className="homeImg">
        <img className="homeImg" src={image} alt="Banner de la página Home" />
    </div>
);