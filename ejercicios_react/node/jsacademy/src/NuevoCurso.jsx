
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';


class NuevoCurso extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            speciality: '',
            volver: false
        };

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }

    //gestión genérica de cambio en campo input
    cambioInput(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    //método activado al enviar el form (submit)
    submit(e) {
        this.props.guardaCurso({
            name: this.state.name,
            speciality: this.state.speciality,
            id: 0
        });
        e.preventDefault();
        this.setState({ volver: true });
    }

    render() {
        //si se activa volver redirigimos a lista
        if (this.state.volver === true) {
            return <Redirect to='/cursos' />
        }

        return (
            <>
            <br></br>
            <br></br>
            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nameInput">Name</Label>
                            <Input type="text" 
                                name="name" 
                                id="nameInput"
                                value={this.state.name}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="specialityInput">Speciality</Label>
                            <Input type="text" name="speciality" id="specialityInput"
                                value={this.state.speciality}
                                onChange={this.cambioInput} />
                        </FormGroup>
                    </Col>
                </Row>


                <Row>
                    <Col>
                        <Button color="primary">Guardar</Button>
                    </Col>
                </Row>
            </Form>
            </>

        );
    }
}






export default NuevoCurso;