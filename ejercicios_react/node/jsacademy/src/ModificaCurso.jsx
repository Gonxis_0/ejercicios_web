
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

// clase idéntica a NuevoContacto
// excepto que recibimos los datos de un contacto existente
// y la lista completa de contactos
class ModificaCurso extends Component {
    constructor(props) {
        super(props);

        //recibimos id a modificar
        let id = this.props.match.params.idCurso * 1;
        //obtenemos el contacto a modificar
        let cursoModificar = this.props.cursos.filter(el => el.id===id)[0];
        //establecemos state con los datos del contacto a modificar
        //incluido id
        this.state = {
            name: cursoModificar.name,
            speciality: cursoModificar.speciality,
            id: cursoModificar.id,
            volver: false
        };

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }

    cambioInput(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }
    
    // el método submit envia también el id para que guardaContacto sepa qué contacto modificar
    submit(e) {
        this.props.guardaCurso({
            name: this.state.name,
            speciality: this.state.speciality,
            id: this.state.id
        });
        e.preventDefault();
        this.setState({ volver: true });
    }

    render() {

        if (this.state.volver === true) {
            return <Redirect to='/cursos' />
        }

        return (

            <>
            <br></br>
            <br></br>
            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nameInput">Name</Label>
                            <Input type="text" 
                                name="name" 
                                id="nameInput"
                                value={this.state.name}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="specialityInput">Speciality</Label>
                            <Input type="text" name="speciality" id="specialityInput"
                                value={this.state.speciality}
                                onChange={this.cambioInput} />
                        </FormGroup>
                    </Col>
                </Row>


                <Row>
                    <Col>
                        <Button color="primary">Guardar</Button>
                    </Col>
                </Row>
            </Form>
            </>

        );
    }
}

export default ModificaCurso;