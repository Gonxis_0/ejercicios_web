import React from "react";

import { Table } from 'reactstrap';
import { Link } from "react-router-dom";

export default class Cursos extends React.Component {

    render() {

        //para crear las filas hacemos un "map" de los alumnos recibidos
        //previamente los ordenamos por id...
        let filasCursos = this.props.cursos.sort((a, b) => a.id - b.id).map(curso => {
            return (
                <tr key={curso.id}>
                    <td>{curso.id}</td>
                    <td>{curso.name}</td>
                    <td>{curso.speciality}</td>
                    <td><Link to={"/modificaCurso/" + curso.id}>Editar</Link></td>
                    <td><Link to={"/eliminaCurso/" + curso.id}>Eliminar</Link></td>
                </tr>
            );
        })


        return (
            <>
                <br></br>
                <br></br>
                <hr></hr>
                <hr></hr>
                <h2>Cursos</h2>
                <Table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Speciality</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {filasCursos}
                    </tbody>
                </Table>
            </>
        );
    }


}

