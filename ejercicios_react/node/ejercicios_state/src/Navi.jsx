import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";

import Thumbs from './Thumbs';
import Tricolor from './Tricolor';
import Foto from './Foto';
import Combo from './Combo';
import { CIUTATS_CAT_20K } from './../../nano01/src/datos';
import Prevision from "./Prevision";
import Cards from "./Cards";

export default class Example extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Navbar color="light" light expand="md">
                        <NavbarBrand href="/">Menú de ejercicios</NavbarBrand>
                        <NavbarToggler onClick={this.toggle} />
                        <Collapse isOpen={this.state.isOpen} navbar>
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                   <NavLink><Link to="Tricolor">Tricolor</Link></NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink><Link to="Foto">Foto</Link></NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink><Link to="Combo">Combo</Link></NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink><Link to="Prevision">Prevision</Link></NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink><Link to="Cards">Cards</Link></NavLink>
                                </NavItem>
                            </Nav>
                        </Collapse>
                    </Navbar>

                    <Switch>
                        <Route path="/Thumbs" render={() => <Thumbs estadoInicial={true} />} />
                        <Route path="/Tricolor" component={Tricolor} />
                        <Route path="/Foto" component={Foto} />
                        <Route path="/Combo" render={() => <Combo campoPrincipal={CIUTATS_CAT_20K} campoSecundario={CIUTATS_CAT_20K} />} />
                        <Route path="/Prevision" component={Prevision} />
                        <Route path="/Cards" component={Cards} />
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}