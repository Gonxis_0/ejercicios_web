import React from 'react';

import './css/combo.css';

export default class Combo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            value: ""
        }
        this.cambiaFiltro = this.cambiaFiltro.bind(this);
    }

    cambiaFiltro(event) {
        this.setState({
            value: event.target.value
        });

        console.log(this.state.municipi);
    }

    render() {
        
        let campoPrincipal = this.props.campoPrincipal
            .map(item => item.comarca)
            .sort();
        
        let uniqCampoPrincipal = [...new Set(campoPrincipal)];
        uniqCampoPrincipal = uniqCampoPrincipal.map(item => <option key={item}>{item}</option>);

        let campoSecundario = this.props.campoSecundario
            .filter(item => item.comarca === this.state.value)
            .map(item => <option key={item.municipi}>{item.municipi}</option>);

        return ( 
            <div className = "combo">
                <select onChange = {this.cambiaFiltro}>
                    <option key="first" selected="selected">Select an option</option>
                    {uniqCampoPrincipal}
                </select>
                <select>
                    <option key="second" selected="selected">Select an option</option>
                    {campoSecundario}
                </select>  
            </div>
        );
    }

}