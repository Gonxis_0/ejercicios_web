import React from 'react';
import './css/tricolor.css';

export default class Tricolor extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            counter: 0,
            arrClasses: ['initialColor', 'redColor', 'greenColor', 'blueColor'],
        }
        this.pulsar = this.pulsar.bind(this);
    }

    pulsar() {
        if (this.state.counter === 3) {
            this.setState({
                counter: 0,
            })
        } else {
            this.setState({
                counter: this.state.counter + 1,
            })
        }
    }

    render() {
        let claseDiv = this.state.arrClasses[this.state.counter];
        return (<div onClick={this.pulsar} className={"tricolor " + claseDiv} >
            </div>
        );
    }

}