import React from 'react';
import './css/foto.css';

import Coche from "./img/Coche.jpg";
import Moto from "./img/Moto.jpg";
import Bici from "./img/Bici.jpg";
import Bus from "./img/Bus.jpg";

export default class Foto extends React.Component{
    
    constructor(props){
        super(props);
        this.state = {
            vehicle: [Coche, Moto, Bici, Bus],
            valor: 2
        }
        this.changeVehicle = this.changeVehicle.bind(this);
    }
    
    changeVehicle(event){
        this.setState({
            valor: event.target.value
        })
    }


    render(){

        let img = this.state.vehicle[this.state.valor];
        return (
            <div className="containerFoto">
                <select onChange={this.changeVehicle} value={this.state.valor}>
                    <option value="0">Coche</option>
                    <option value="1">Moto</option>
                    <option value="2">Bici</option>
                    <option value="3">Bus</option>
                </select>
                <img src={img} />
            </div>
        );
    }

}