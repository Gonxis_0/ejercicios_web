import React from 'react';

import {Container, Row, Col, Table} from 'reactstrap';
import './css/prevision.css';


export default class Prevision extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        };

        this.getPrevisio = this.getPrevisio.bind(this);
        this.getPrevisio();
    }

    getPrevisio(){
        //api.openweathermap.org/data/2.5/forecast/daily?q={city name}&cnt=7&APPID=${apiKey}&units=metric

        const ciutat = "barcelona,es";
        const apiKey = "84dbcf8c3480649bce9d4bb58da44b4e";
        const funcio = "forecast";
        const apiUrl = `http://api.openweathermap.org/data/2.5/${funcio}?q=${ciutat}&APPID=${apiKey}&units=metric`;
        //const apiURL2 = `http://api.openweathermap.org/data/2.5/forecast/daily?q=${ciutat}&cnt=7&APPID=${apiKey}&units=metric`;

        fetch(apiUrl)
            .then(response => response.json())
            .then(data => this.setState( data ))
            .catch(error => console.log(error));
    }

    render() {

        if (!this.state.list.length) {
            return <h1>Cargando datos...</h1>
        }

        let fecha1 = new Date(this.state.list[0].dt * 1000);
        let fecha2 = new Date(this.state.list[7].dt * 1000);
        let fecha3 = new Date(this.state.list[15].dt * 1000);
        let fecha4 = new Date(this.state.list[23].dt * 1000);
        let fecha5 = new Date(this.state.list[31].dt * 1000);
        console.log(this.state.list);

        return (
            <div className="prevision">
            <h3>Previsión para {fecha1.toString()}</h3>
            <h3>Temperatura {this.state.list[0].main.temp}</h3>
     
            <h3>Previsión para {fecha2.toString()}</h3>
            <h3>Temperatura {this.state.list[7].main.temp}</h3>

            <h3>Previsión para {fecha3.toString()}</h3>
            <h3>Temperatura {this.state.list[15].main.temp}</h3>
     
            <h3>Previsión para {fecha4.toString()}</h3>
            <h3>Temperatura {this.state.list[23].main.temp}</h3>

            <h3>Previsión para {fecha5.toString()}</h3>
            <h3>Temperatura {this.state.list[31].main.temp}</h3>
     
            </div>
        );
    }

}