function test() {
    var total = 0;
    do {
        num = prompt("entra numero") * 1;
        total = total + num;
    } while (num !== 0);
    console.log(total);
}

function minmax() {
    let arr = [];
    let num;
    do {
        num = prompt("entra numero") * 1;
        if (num !== 0) {
            arr.push(num);
        }
    } while (num !== 0);

    arr.sort();
    let len = arr.length - 1;
    console.log(`El min: ${arr[0]}, el max: ${arr[len]}`);
}

let avg = () => {
    let arr = [];
    let num;
    do {
        num = prompt("entra numero") * 1;
        if (num !== 0) {
            arr.push(num);
        }
    } while (num !== 0);

    num = 0;
    for (let i = 0; i < arr.length; i++) {
        num = num + arr[i];
    }

    let average = num / arr.length;
    console.log(`La media es: ${num}/${arr.length} =  ${average}`);
}

let bisiesto = (year = 2019) => {
    let flag = false;

    if (year % 4 === 0) {
        if (year % 100 === 0 && year % 400 !== 0) {
            flag = false;
        }
        flag = true;
    } else {
        flag = false;
    }

    return flag;
}

let today = () => {
    let hoy = new Date();
    let year = hoy.getFullYear();
    let month = hoy.getMonth();
    let day = hoy.getDate();

    meses = ["Enero", "Febrero", "Marzo", "Abril"]

    console.log(`${day} de ${meses[month]} de ${year}`);
}

let diasPasados = (fecha1, fecha2) => {

    let f1 = fecha1.getTime();
    let f2 = fecha2.getTime();

    let diasQueHanPasado = (Math.abs(f1 - f2) / 1000 / 60 / 60 / 24);

    return console.log(`Han pasado ${diasQueHanPasado} días entre la fecha ${f1} y la fecha ${f2}`);
}

let adivinaNum = () => {
    let counter = 0;
    let numRandom = parseInt(Math.random() * 101);
    let num;

    do {
        num = prompt("Introduce un número. Si quieres salir, introduce el -1") * 1;
        counter++;

        if (num < numRandom && num !== -1) {
            console.log(`El número ${num} es mas pequeño que el número que estamos buscando`);
        } else if (num > numRandom) {
            console.log(`El número ${num} es mas grande que el número que estamos buscando`);
        } else if (num === numRandom) {
            return `Muy bien. Has encontrado el número en un total de ${counter} intentos.`;
        }
    } while (num !== numRandom && num !== -1);
}

let numMayor = (num1, num2) => {
    if (num1 < num2) {
        return `${num1} es menor a ${num2}`;
    } else if (num2 < num1) {
        return `${num1} es mayor a ${num2}`;
    } else {
        return 'Los dos numeros son iguales';
    }
}

let datosDeNumero = (num) => {
    let objNum = {
        par: false,
        impar: true,
        divisiblePor3: false,
        divisiblePor5: false,
        divisiblePor7: false
    };
    if (num % 2 === 0) {
        objNum.par = true;
        objNum.impar = false;
    } else {
        objNum.par = false;
        objNum.impar = true;
    }

    if (num % 3 === 0) {
        objNum.divisiblePor3 = true;
    } else {
        objNum.divisiblePor3 = false;
    }

    if (num % 5 === 0) {
        objNum.divisiblePor5 = true;
    } else {
        objNum.divisiblePor5 = false;
    }

    if (num % 7 === 0) {
        objNum.divisiblePor7 = true;
    } else {
        objNum.divisiblePor7 = false;
    }

    return objNum;
}

let sumaValores = (arr) => {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        sum += arr[i];
    }

    return sum;
}

let factorial = (num) => {
    let total = 1;
    for (i = 1; i <= num; i++) {
        total = total * i;
    }
    return total;
}

function primo2() {
    let c = 100;
    let j = 2;
    let numerosPrimos = [];

    for (; j < c; j++) {

        if (primo1(j)) {
            numerosPrimos.push(j);
        }

    }

    console.log(numerosPrimos);

    function primo1(numero) {

        for (let i = 2; i < numero; i++) {

            if (numero % i === 0) {
                return false;
            }

        }

        return numero !== 1;
    }
}

let primo = (numero) => {
    console.log("Has pasado el numero: " + numero);
    console.log("Inicio bucle desde 2 hasta " + (numero - 1));

    for (var i = 2; i < numero; i++) {

        console.log("Modulo entre " + numero + " y " + i + " = " + (numero % i));

        if (numero % i === 0) {
            console.log(i + " es un multiplo de " + numero);
            console.log(numero + " no es un numero primo porque " + i + " es un multiplo");
            return false;
        }

    }

    if (numero === 1) {
        console.log("Me has pasado el numero 1, recuerda que NO es un numero primo");
    } else {
        console.log("Como el numero ingresado no tuvo mas múltiplos entonces determinamos que SI es un numero primo.");
    }

    console.log("-------------------------------------");
};

let fibonacci = (num) => {
    //if (num <= 1) return 1;
    //return fibonacci(num - 1) + fibonacci(num - 2);

    let a = 1,
        b = 0,
        temp,
        arr = [];

    while (num >= 0) {
        temp = a;
        arr.push(temp);
        a = a + b;
        b = temp;
        num--;
    }

    return console.log(arr, b);

};

let capitaliza = (x) => {
    let upCase = x.substring(0, 1);
    let lowCase = x.substring(1, x.length);
    upCase = upCase.toUpperCase();
    lowCase = lowCase.toLowerCase();

    return upCase + lowCase;
}

let diaActual = () => {
    let hoy = new Date();
    let xMasDay = new Date(2019 - 12 - 24);

    let actualDayNum = hoy.getDate();
    let actualDayString = hoy.getDay();
    let month = hoy.getMonth();
    let year = hoy.getFullYear();
    let millisecondDay = hoy.getMilliseconds();
    let millisecondXMasDay = xMasDay.getMilliseconds();

    let dias = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

    let diasFaltanNavidadMilisegundos = millisecondXMasDay - millisecondDay;
    let diasFaltanNavidad = diasFaltanNavidadMilisegundos;

    return `Hoy es día ${dias[actualDayString]} ${actualDayNum} de ${meses[month]} del ${year} y faltan ${diasFaltanNavidad} días para Navidad`;
}