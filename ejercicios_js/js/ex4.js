const MOTOS = require("./MOTOS.js");

function ordenarPorPrecio(a, b) {
    if (a.preu > b.preu) {
        return 1;
    } else if (a.preu < b.preu) {
        return -1;
    } else return 0;
}

let filtroKmHonda = MOTOS.filter(el => {
    let marcaSplit = el.model.split(' ');
    let marca = marcaSplit[0];
    let km = el.kilometres;
    return (km < 30000 && marca === "HONDA");
});

let filtroKmCc = MOTOS.filter(el => {
    let km = el.kilometres;
    let cc = el.cilindrada;
    return (km < 30000 && cc > 240);
});

let motoUnica = MOTOS.filter(el => {
    let km = el.kilometres;
    let cc = el.cilindrada;
    let price = el.preu;
    return (km < 25000 && cc > 350 && (price > 1800 && price < 2200));
});

let getMarca = el => {
    let marcaSplit = el.model.split(' ');
    let marca = marcaSplit[0];
    return marca;
}

let marcasArr = [];
let marcas = MOTOS.forEach(el => {
    let marca = getMarca(el);
    if (marcasArr.indexOf(marca) === -1) {
        marcasArr.push(marca);
    }
});

let parque = marcasArr.map(marca => {
    let listaProvisional = MOTOS.filter(moto => getMarca(moto) === marca);
    let motos = listaProvisional.length;

    return { marca_moto: marca, num_motos: motos };
});

//MOTOS.sort(ordenarPorPrecio);
//console.log(`La moto más barata cuesta: ${MOTOS[0].preu} y es este objeto: ${MOTOS[0]} y la más cara cuesta: ${MOTOS[MOTOS.length-1].preu} y es este objeto: ${MOTOS[MOTOS.length-1]}`);
console.log("");
console.log("");
console.log("El filtrado de los Km con la marca HONDA da lo siguiente: ");
console.log("----------------------------------------------------------");
console.log(filtroKmHonda);
console.log("----------------------------------------------------------");
console.log(`Total de resultados: ${filtroKmHonda.length}`);
console.log("----------------------------------------------------------");
console.log(" ");
console.log(" ");
console.log("El filtrado de los Km con las cilindradas da lo siguiente: ");
console.log("-----------------------------------------------------------");
console.log(filtroKmCc);
console.log("----------------------------------------------------------");
console.log(`Total de resultados: ${filtroKmCc.length}`);
console.log("----------------------------------------------------------");
console.log(" ");
console.log(" ");
console.log("El filtrado de las motos entre un precio y otro son: ");
console.log("-----------------------------------------------------------");
console.log(motoUnica);
console.log("----------------------------------------------------------");
console.log(`Total de resultados: ${motoUnica.length}`);
console.log("----------------------------------------------------------");
console.log(" ");
console.log(" ");
console.log("El filtrado del parque de motos es: ");
console.log("-----------------------------------------------------------");
console.log(parque);
console.log("----------------------------------------------------------");
console.log(" ");
console.log(" ");