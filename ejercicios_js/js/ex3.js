class Figura {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    posicion() {
        console.log(`(Posición x: ${this.x}, Posición y: ${this.y})`);
    }
}

class Rectangulo extends Figura {
    constructor(x, y, lado1, lado2) {
        super(x, y);
        this.lado1 = lado1;
        this.lado2 = lado2;
    }

    calcArea() {
        return `El area es ` + this.lado1 * this.lado2;
    }
}

class Triangulo extends Figura {
    constructor(x, y, base, altura) {
        super(x, y);
        this.base = base;
        this.altura = altura;
    }

    calcArea() {
        let area = (this.base * this.altura) / 2;
        return `El area del triangulo es ${area}`;
    }
}

class Cuadrado extends Rectangulo {
    constructor(x, y, lado1) {
        super(x, y, lado1, lado1);
    }

    calcArea() {
        return `El area del cuadrado es ` + this.lado1 * this.lado1;
    }
}

let square = new Cuadrado(0, 0, 10);
let triangle = new Triangulo(0, 0, 10, 3);
let rectangle = new Rectangulo(0, 0, 10, 5);