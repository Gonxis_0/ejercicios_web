let changePage = 1;
let computerGame;
let objPiedraPapelTijera = {
    0: "Piedra",
    1: "Papel",
    2: "Tijeras"
};

$(document).on("click", "div.circular", function() {
    $(this).toggleClass("changeColor");
});

$(document).on("click", ".putRed", function() {
    $("div.circular").addClass("red");
});

$(document).on("click", ".notRed", function() {
    $("div.circular").removeClass("red");
});

$(document).on("click", ".copyText", function() {
    let str = $(".inputToCopy").val();
    $(".inputToPaste").val(str);
});

$(document).on("click", ".replace", function() {
    let str = $(".replaceInTable").val();
    $(".reemplazado").html(str);
});

$(document).on("click", ".add", function() {
    let str = $(".replaceInTable").val();
    $(".reemplazado").append(str);
});

$(document).on("click", ".plus", function() {
    let counter = $(".number").val();
    if (counter < 10) {
        counter++;
    }
    $(".number").val(counter);
});

$(document).on("click", ".minus", function() {
    let counter = $(".number").val();
    if (counter > 0) {
        counter--;
    }
    $(".number").val(counter);
});
/*
$(document).on("click", ".prev", function() {
    console.log("Estoy en la función previous!");
    let counter = $("a.it_1").text() * 1;
    if (counter > 0) {
        counter = $("a.it_1").text();
        counter--;
        $(".it_1").text(counter);

        counter = $("a.it_2").text();
        counter--;
        $(".it_2").text(counter);

        counter = $("a.it_3").text();
        counter--;
        $(".it_3").text(counter);

        counter = $("a.it_4").text();
        counter--;
        $(".it_4").text(counter);

        counter = $("a.it_5").text();
        counter--;
        $(".it_5").text(counter);

        changePage--;
    }
});

$(document).on("click", ".nex", function() {
    console.log("Estoy en la función next!");
    let counter = $("a.it_5").text() * 1;
    if (counter < 20) {
        counter = $("a.it_1").text();
        counter++;
        $(".it_1").text(counter);

        counter = $("a.it_2").text();
        counter++;
        $(".it_2").text(counter);

        counter = $("a.it_3").text();
        counter++;
        $(".it_3").text(counter);

        counter = $("a.it_4").text();
        counter++;
        $(".it_4").text(counter);

        counter = $("a.it_5").text();
        counter++;
        $(".it_5").text(counter);

        changePage++;
    }
});
*/
$(document).on("click", "#nex", function() {

    let valorUltimo = parseInt($("#ultimo").text());
    if (valorUltimo < 20) {
        $(".numPagina").each(function() {
            let actual = parseInt($(this).text());
            actual++;
            changePage++;
            $(this).text(actual);
            $(this).attr("href", actual);
        });
    }
});

$(document).on("click", "#prev", function() {
    let valorPrimero = parseInt($("#primero").text());
    if (valorPrimero > 0) {
        $(".numPagina").each(function() {
            let actual = parseInt($(this).text());
            actual--;
            changePage--;
            $(this).text(actual);
            $(this).attr("href", actual);
        });
    }
});

/*
$(document).on("click", "a.page-link", function() {
    let counter = $(this).attr('class');
    let num = counter.substring(counter.length - 1);
    let page = (parseInt(num) + changePage) - 1;
    if (num !== "NaN" && num !== "k") {
        if (changePage === 1) {
            alert('Redireccionamiento a la página ' + num);
            //$(this).attr("href", num);
        } else {
            alert('Redireccionamiento a la página ' + page);
            //$(this).attr("href", page);
        }
    }
});
*/

$(document).on("click", ".calc", function() {
    let valor1 = Math.floor(Math.random() * 50);
    let valor2 = Math.floor(Math.random() * 50);
    let valor3 = Math.floor(Math.random() * 50);
    console.log(valor1, valor2, valor3);

    $(".circular2_1").html(valor1);
    $(".circular2_2").html(valor2);
    $(".circular2_3").html(valor3);
});

$(document).on("click", ".piedra", function() {
    computerGame = Math.floor(Math.random() * 3);
    let answer = objPiedraPapelTijera[computerGame];
    if (answer === "Tijeras") {
        console.log(`El ordenador ha jugado ${answer} y tu Piedra, has ganado!`);
    } else if (answer === "Papel") {
        console.log(`El ordenador ha jugado ${answer} y tu Piedra, has perdido...`);
    } else {
        console.log(`Los dos os habéis puesto de acuerdo en sacar ${answer}, habéis empatado`);
    }
});

$(document).on("click", ".papel", function() {
    computerGame = Math.floor(Math.random() * 3);
    let answer = objPiedraPapelTijera[computerGame];
    if (answer === "Piedra") {
        console.log(`El ordenador ha jugado ${answer} y tu Papel, has ganado!`);
    } else if (answer === "Tijeras") {
        console.log(`El ordenador ha jugado ${answer} y tu Papel, has perdido...`);
    } else {
        console.log(`Los dos os habéis puesto de acuerdo en sacar ${answer}, habéis empatado`);
    }
});

$(document).on("click", ".tijeras", function() {
    computerGame = Math.floor(Math.random() * 3);
    let answer = objPiedraPapelTijera[computerGame];
    if (answer === "Papel") {
        console.log(`El ordenador ha jugado ${answer} y tu Tijeras, has ganado!`);
    } else if (answer === "Piedra") {
        console.log(`El ordenador ha jugado ${answer} y tu Tijeras, has perdido...`);
    } else {
        console.log(`Los dos os habéis puesto de acuerdo en sacar ${answer}, habéis empatado`);
    }
});