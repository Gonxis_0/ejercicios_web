var url = "https://api.citybik.es/v2/networks/bicing";
let map;
let coords = []; // array de objetos lat,long creado a partir de estaciones
let markers = [];

function initMap() {
    var center = {
        lat: 41.408672,
        lng: 2.174464
    };
    map = new google.maps.Map(
        document.getElementById('map'), {
            zoom: 12,
            center: center
        });
};

function addMarkersToMap() {
    coords.forEach(function(element) {
        var latlng = new google.maps.LatLng(element.latitude, element.longitude);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map
        });
        markers.push(marker);
    });
};

$(document).on("click", "#boton1", function() {
    $("table.mitabla tbody").html('');
    let inputVar = parseInt($("input#minimumBikes").val());

    if (inputVar) {
        console.log("Aquí hay valor");
    } else {
        console.log("No hay valor en el input");
    }

    $.getJSON(url, function(data) {

        stations = data.network.stations;

        for (i = 0; i < stations.length; i++) {
            actual = stations[i];
            // console.log(actual.title);
            if (inputVar) {
                if (actual.free_bikes >= inputVar) {
                    fila = $("<tr>");
                    col1 = $("<td>").text(actual.name);
                    col2 = $("<td>").text(actual.free_bikes);
                    col3 = $("<td>").text(actual.empty_slots);
                    col4 = $("<td>").text(actual.latitude);
                    col5 = $("<td>").text(actual.longitude);
                    fila.append(col1, col2, col3, col4, col5);
                    $("table.mitabla tbody").append(fila);

                    coords.push({
                        latitude: actual.latitude,
                        longitude: actual.longitude
                    });
                } else {
                    console.log(`No hay tantas bicis en la estación ${actual.name}. Prueba con un número más pequeño`);
                }
            } else {
                fila = $("<tr>");
                col1 = $("<td>").text(actual.name);
                col2 = $("<td>").text(actual.free_bikes);
                col3 = $("<td>").text(actual.empty_slots);
                col4 = $("<td>").text(actual.latitude);
                col5 = $("<td>").text(actual.longitude);
                fila.append(col1, col2, col3, col4, col5);
                $("table.mitabla tbody").append(fila);

                coords.push({
                    latitude: actual.latitude,
                    longitude: actual.longitude
                });
            }
        }

        addMarkersToMap();
    });
})