let express = require('express');
let app = express();

let bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/', (request, response) => {
    response.send(`
    <h1>Title</h1>
    <a href="/km">Kms to miles</a>
    <br />
    <a href="/km">kms to meters</a>
    `);
});

app.get('/km', (request, response) => {
    response.send(`
        <form action="/millas" method="post">
            <label>Entra Km</label>
            <input name="kms">
            <button>Enviar a millas</button>
        </form>

        <form action="/meter" method="post">
            <label>Entra Km</label>
            <input name="kms">
            <button>Enviar a metros</button>
        </form>
    `);
});

app.post('/millas', (request, response) => {
    let kms = request.body.kms;
    let millas = kms * 1.6;
    response.send(`
        <h2>${kms} km = ${millas} millas</h2>
    `);
});

app.post('/meter', (request, response) => {
    let kms = request.body.kms;
    let meter = kms * 1000;
    response.send(`
        <h2>${kms} km = ${meter} metros</h2>
    `);
}) ;

app.listen(3004, () => {
    console.log("Web online en 3004");
});