exports.areaCuadrado = (lado) => lado * lado;
exports.areaCirculo = (radio) => radio * radio * 3.141516;

exports.MIL = 1000;
