'use strict';

const distancia = require('./distancia');
const velocidad = require('./velocidad');

const valor = process.argv[2];
const desde = process.argv[3];
const hacia = process.argv[5];

//Distancia
if (desde === 'km' && hacia === 'm') {
    console.log(`${valor}km = ${distancia.kmTOm(valor)}m`);
} else if (desde === 'km' && hacia === 'millas') {
    console.log(`${valor}km = ${distancia.kmTOmi(valor)}millas`);
} else if (desde === 'm' && hacia === 'km') {
    console.log(`${valor}m = ${distancia.mTOkm(valor)}km`);
} else if (desde === 'm' && hacia === 'millas') {
    console.log(`${valor}m = ${distancia.mTOmi(valor)}millas`);
} else if (desde === 'millas' && hacia === 'km') {
    console.log(`${valor}millas = ${distancia.miTOkm(valor)}km`);
} else if (desde === 'millas' && hacia === 'm') {
    console.log(`${valor}millas = ${distancia.miTOm(valor)}m`);
// Velocidad
} else if (desde === 'kmxh' && hacia === 'mxs') {
    console.log(`${valor}km/h = ${velocidad.kmxhTOmxs(valor)}m/s`);
} else if (desde === 'kmxh' && hacia === 'millasxh') {
    console.log(`${valor}km/h = ${velocidad.kmxhTOmixh(valor)}millas/h`);
} else if (desde === 'mxs' && hacia === 'kmxh') {
    console.log(`${valor}m/s = ${velocidad.mxsTOkmxh(valor)}km/h`);
} else if (desde === 'mxs' && hacia === 'millasxh') {
    console.log(`${valor}m/s = ${velocidad.mxsTOmixh(valor)}millas/h`);
} else if (desde === 'millasxh' && hacia === 'kmxh') {
    console.log(`${valor}millas/h = ${velocidad.mixhTOkmxh(valor)}km/h`);
} else if (desde === 'millasxh' && hacia === 'mxs') {
    console.log(`${valor}millas/h = ${velocidad.mixhTOmxs(valor)}m/s`);
} else {
    console.log("Los valores admitidos son m, km y millas para las distancias o millasxh, kmxh y mxs para las velocidades");
}