'use strict';

let geom = require('./geometria');

const figura = process.argv[2];
const valor = process.argv[3];

if (figura == 'cuadrado') {
    console.log(`Area de cuadrado de ${valor} = ${geom.areaCuadrado(valor*1)}cm2`);
} else if (figura == 'circulo') {
    console.log(`Area de circulo de 4cm = ${geom.areaCirculo(valor*1)}cm2`);
} else {
    console.log("Error...")
}

//console.log(`Area de cuadrado de 8cm = ${geom.areaCuadrado(8)}cm2`);
//console.log(`Area de circulo de 4cm = ${geom.areaCirculo(4)}cm2`);

console.log(geom.MIL);

console.log(process.argv);