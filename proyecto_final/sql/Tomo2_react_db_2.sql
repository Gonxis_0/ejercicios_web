-- MySQL Workbench Synchronization
-- Generated: 2019-05-12 09:58
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Gonxi's

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

ALTER TABLE `Tomo2_react_db`.`contest` 
ADD INDEX `fk_contest_user_idx` (`user_id` ASC),
DROP INDEX `fk_contest_user_idx` ;
;

ALTER TABLE `Tomo2_react_db`.`question` 
ADD INDEX `fk_question_contest1_idx` (`contest_id` ASC, `contest_user_id` ASC),
DROP INDEX `fk_question_contest1_idx` ;
;

ALTER TABLE `Tomo2_react_db`.`answer` 
ADD INDEX `fk_answer_question1_idx` (`question_id` ASC, `question_contest_id` ASC, `question_contest_user_id` ASC),
ADD INDEX `fk_answer_contest1_idx` (`contest_id` ASC, `contest_user_id` ASC),
DROP INDEX `fk_answer_contest1_idx` ,
DROP INDEX `fk_answer_question1_idx` ;
;

CREATE TABLE IF NOT EXISTS `Tomo2_react_db`.`booking` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `from_date` TIMESTAMP NOT NULL,
  `until_date` TIMESTAMP NOT NULL,
  `type` VARCHAR(45) DEFAULT NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `user_id`),
  INDEX `fk_booking_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_booking_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `Tomo2_react_db`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `Tomo2_react_db`.`trolley` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `booking_id` INT(11) NOT NULL,
  `booking_user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `booking_id`, `booking_user_id`),
  INDEX `fk_trolley_booking1_idx` (`booking_id` ASC, `booking_user_id` ASC),
  CONSTRAINT `fk_trolley_booking1`
    FOREIGN KEY (`booking_id` , `booking_user_id`)
    REFERENCES `Tomo2_react_db`.`booking` (`id` , `user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `Tomo2_react_db`.`new` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `Tomo2_react_db`.`shop` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `Tomo2_react_db`.`product` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `image` VARCHAR(500) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `Tomo2_react_db`.`carrito` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `Tomo2_react_db`.`reservation` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `carrito_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `user_id`, `carrito_id`),
  INDEX `fk_reservation_user1_idx` (`user_id` ASC),
  INDEX `fk_reservation_carrito1_idx` (`carrito_id` ASC),
  CONSTRAINT `fk_reservation_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `Tomo2_react_db`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservation_carrito1`
    FOREIGN KEY (`carrito_id`)
    REFERENCES `Tomo2_react_db`.`carrito` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `Tomo2_react_db`.`reservation_has_booking` (
  `reservation_id` INT(11) NOT NULL,
  `reservation_user_id` INT(11) NOT NULL,
  `reservation_carrito_id` INT(11) NOT NULL,
  `booking_id` INT(11) NOT NULL,
  `booking_user_id` INT(11) NOT NULL,
  PRIMARY KEY (`reservation_id`, `reservation_user_id`, `reservation_carrito_id`, `booking_id`, `booking_user_id`),
  INDEX `fk_reservation_has_booking_booking1_idx` (`booking_id` ASC, `booking_user_id` ASC),
  INDEX `fk_reservation_has_booking_reservation1_idx` (`reservation_id` ASC, `reservation_user_id` ASC, `reservation_carrito_id` ASC),
  CONSTRAINT `fk_reservation_has_booking_reservation1`
    FOREIGN KEY (`reservation_id` , `reservation_user_id` , `reservation_carrito_id`)
    REFERENCES `Tomo2_react_db`.`reservation` (`id` , `user_id` , `carrito_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservation_has_booking_booking1`
    FOREIGN KEY (`booking_id` , `booking_user_id`)
    REFERENCES `Tomo2_react_db`.`booking` (`id` , `user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `Tomo2_react_db`.`contest` 
ADD CONSTRAINT `fk_contest_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `Tomo2_react_db`.`user` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `Tomo2_react_db`.`question` 
ADD CONSTRAINT `fk_question_contest1`
  FOREIGN KEY (`contest_id` , `contest_user_id`)
  REFERENCES `Tomo2_react_db`.`contest` (`id` , `user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `Tomo2_react_db`.`answer` 
ADD CONSTRAINT `fk_answer_question1`
  FOREIGN KEY (`question_id` , `question_contest_id` , `question_contest_user_id`)
  REFERENCES `Tomo2_react_db`.`question` (`id` , `contest_id` , `contest_user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_answer_contest1`
  FOREIGN KEY (`contest_id` , `contest_user_id`)
  REFERENCES `Tomo2_react_db`.`contest` (`id` , `user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
