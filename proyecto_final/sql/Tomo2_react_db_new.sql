-- MySQL Workbench Synchronization
-- Generated: 2019-05-14 10:23
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Gonxi's

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `tomo2_react_db_new` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXISTS `tomo2_react_db_new`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `surname` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `genre` VARCHAR(45) NOT NULL,
  `isAdmin` TINYINT(1) NOT NULL DEFAULT 0,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tomo2_react_db_new`.`booking` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `from_date` TIMESTAMP NOT NULL,
  `until_date` TIMESTAMP NOT NULL,
  `type` VARCHAR(100) NOT NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` INT(11) NOT NULL,
  `carrito_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `user_id`, `carrito_id`),
  INDEX `fk_booking_user_idx` (`user_id` ASC),
  INDEX `fk_booking_carrito1_idx` (`carrito_id` ASC),
  CONSTRAINT `fk_booking_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `tomo2_react_db_new`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_carrito1`
    FOREIGN KEY (`carrito_id`)
    REFERENCES `tomo2_react_db_new`.`carrito` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tomo2_react_db_new`.`carrito` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `description` VARCHAR(600) NOT NULL,
  `type` VARCHAR(100) NOT NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
