'use strict';
module.exports = function(app) {
    let user = require('../controller/userController');
    let booking = require('../controller/bookingController');
    let carrito = require('../controller/carritoController');
    let reservation = require('../controller/reservationController');
    let reservationHasBooking = require('../controller/reservationHasBookingController');
    let trolley = require('../controller/trolleyController');

// Tomo 2 React Project Routes

    //User
    app.route('/user')
        .get(user.list_all_users)
        .post(user.create_a_user);

    app.route('/user/:userId')
        .get(user.read_a_user)
        .put(user.update_a_user)
        .delete(user.delete_a_user);

    //Booking
    app.route('/booking')
        .get(booking.list_all_bookings)
        .post(booking.create_a_booking);

    app.route('/booking/:bookingId')
        .get(booking.read_a_booking)
        .put(booking.update_a_booking)
        .delete(booking.delete_a_booking);

    //Carrito
    app.route('/carrito')
        .get(carrito.list_all_carritos)
        .post(carrito.create_a_carrito);

    app.route('/carrito/:carritoId')
        .get(carrito.read_a_carrito)
        .put(carrito.update_a_carrito)
        .delete(carrito.delete_a_carrito);

    //Reservation
    app.route('/reservation')
        .get(reservation.list_all_reservations)
        .post(reservation.create_a_reservation);

    app.route('/reservation/:reservationId')
        .get(reservation.read_a_reservation)
        .put(reservation.update_a_reservation)
        .delete(reservation.delete_a_reservation);

    //Reservation_has_booking
    app.route('/reservationHasBooking')
        .get(reservationHasBooking.list_all_reservationsHasBooking)
        .post(reservationHasBooking.create_a_reservationHasBooking);

    app.route('/reservationHasBooking/:reservationHasBookingId')
        .get(reservationHasBooking.read_a_reservationHasBooking)
        .put(reservationHasBooking.update_a_reservationHasBooking)
        .delete(reservationHasBooking.delete_a_reservationHasBooking);

    //Trolley
    app.route('/trolley')
        .get(trolley.list_all_trolleys)
        .post(trolley.create_a_trolley);

    app.route('/trolley/:trolleyId')
        .get(trolley.read_a_trolley)
        .put(trolley.update_a_trolley)
        .delete(trolley.delete_a_trolley);
};