'use strict';

let Booking = require('../model/bookingModel.js');

exports.list_all_bookings = function(req, res) {
    Booking.getAllBooking(function(err, booking) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', booking);
        res.send(booking);
    });
};



exports.create_a_booking = function(req, res) {
    let new_booking = new Booking(req.body);

    //handles null error 
    if (!new_booking.from_date || !new_booking.until_date || !new_booking.type) {

        res.status(400).send({ error: true, message: 'Please provide from_date/until_date/type' });

    } else {

        Booking.createBooking(new_booking, function(err, booking) {

            if (err)
                res.send(err);
            res.json(booking);
        });
    }
};


exports.read_a_booking = function(req, res) {
    Booking.getBookingById(req.params.bookingId, function(err, booking) {
        if (err)
            res.send(err);
        res.json(booking);
    });
};


exports.update_a_booking = function(req, res) {
    Booking.updateById(req.params.bookingId, new Booking(req.body), function(err, booking) {
        if (err)
            res.send(err);
        res.json(booking);
    });
};


exports.delete_a_booking = function(req, res) {


    Booking.remove(req.params.bookingId, function(err, booking) {
        if (err)
            res.send(err);
        res.json({ message: 'Booking successfully deleted' });
    });
};