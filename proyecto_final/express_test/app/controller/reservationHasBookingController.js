'use strict';

let ReservationHasBooking = require('../model/reservationHasBookingModel.js');

exports.list_all_reservationsHasBooking = function(req, res) {
    ReservationHasBooking.getAllReservationHasBooking(function(err, reservationHasBooking) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', reservationHasBooking);
        res.send(reservationHasBooking);
    });
};



exports.create_a_reservationHasBooking = function(req, res) {
    let new_reservationHasBooking = new ReservationHasBooking(req.body);

    //handles null error 
    if (!new_reservationHasBooking.reservation_id || !new_reservationHasBooking.reservation_user_id || !new_reservationHasBooking.reservation_carrito_id || !new_reservationHasBooking.booking_id || !new_reservationHasBooking.booking_user_id) {

        res.status(400).send({ error: true, message: 'Please provide reservation_id/reservation_user_id/reservation_carrito_id/booking_id/booking_user_id' });

    } else {

        ReservationHasBooking.createReservationHasBooking(new_reservationHasBooking, function(err, reservationHasBooking) {

            if (err)
                res.send(err);
            res.json(reservationHasBooking);
        });
    }
};


exports.read_a_reservationHasBooking = function(req, res) {
    ReservationHasBooking.getReservationHasBookingById(req.params.reservationHasBookingId, function(err, reservationHasBooking) {
        if (err)
            res.send(err);
        res.json(reservationHasBooking);
    });
};


exports.update_a_reservationHasBooking = function(req, res) {
    ReservationHasBooking.updateById(req.params.reservationHasBookingId, new ReservationHasBooking(req.body), function(err, reservationHasBooking) {
        if (err)
            res.send(err);
        res.json(reservationHasBooking);
    });
};


exports.delete_a_reservationHasBooking = function(req, res) {
    ReservationHasBooking.remove(req.params.reservationHasBookingId, function(err, reservationHasBooking) {
        if (err)
            res.send(err);
        res.json({ message: 'Reservation_has_booking successfully deleted' });
    });
};