'use strict';

let Trolley = require('../model/trolleyModel.js');

exports.list_all_trolleys = function(req, res) {
    Trolley.getAllTrolley(function(err, trolley) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', trolley);
        res.send(trolley);
    });
};



exports.create_a_trolley = function(req, res) {
    let new_trolley = new Trolley(req.body);

    //handles null error 
    if (!new_trolley.description || !new_trolley.type || !new_trolley.booking_id || !new_trolley.booking_user_id) {

        res.status(400).send({ error: true, message: 'Please provide description/type/booking_id/booking_user_id' });

    } else {

        Trolley.createTrolley(new_trolley, function(err, trolley) {

            if (err)
                res.send(err);
            res.json(trolley);
        });
    }
};


exports.read_a_trolley = function(req, res) {
    Trolley.getTrolleyById(req.params.trolleyId, function(err, trolley) {
        if (err)
            res.send(err);
        res.json(trolley);
    });
};


exports.update_a_trolley = function(req, res) {
    Trolley.updateById(req.params.trolleyId, new Trolley(req.body), function(err, trolley) {
        if (err)
            res.send(err);
        res.json(trolley);
    });
};


exports.delete_a_trolley = function(req, res) {
    Trolley.remove(req.params.trolleyId, function(err, trolley) {
        if (err)
            res.send(err);
        res.json({ message: 'Trolley successfully deleted' });
    });
};