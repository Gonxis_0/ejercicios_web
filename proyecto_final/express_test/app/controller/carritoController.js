'use strict';

let Carrito = require('../model/carritoModel.js');

exports.list_all_carritos = function(req, res) {
    Carrito.getAllCarrito(function(err, carrito) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', carrito);
        res.send(carrito);
    });
};



exports.create_a_carrito = function(req, res) {
    let new_carrito = new Carrito(req.body);

    //handles null error 
    if (!new_carrito.title || !new_carrito.description || !new_carrito.type) {

        res.status(400).send({ error: true, message: 'Please provide title/description/type' });

    } else {

        Carrito.createCarrito(new_carrito, function(err, carrito) {

            if (err)
                res.send(err);
            res.json(carrito);
        });
    }
};


exports.read_a_carrito = function(req, res) {
    Carrito.getCarritoById(req.params.carritoId, function(err, carrito) {
        if (err)
            res.send(err);
        res.json(carrito);
    });
};


exports.update_a_carrito = function(req, res) {
    Carrito.updateById(req.params.carritoId, new Carrito(req.body), function(err, carrito) {
        if (err)
            res.send(err);
        res.json(carrito);
    });
};


exports.delete_a_carrito = function(req, res) {
    Carrito.remove(req.params.carritoId, function(err, carrito) {
        if (err)
            res.send(err);
        res.json({ message: 'Carrito successfully deleted' });
    });
};