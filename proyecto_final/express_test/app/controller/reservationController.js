'use strict';

let Reservation = require('../model/reservationModel.js');

exports.list_all_reservations = function(req, res) {
    Reservation.getAllReservation(function(err, reservation) {

        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', reservation);
        res.send(reservation);
    });
};



exports.create_a_reservation = function(req, res) {
    let new_reservation = new Reservation(req.body);

    //handles null error 
    if (!new_reservation.user_id || !new_reservation.carrito_id) {

        res.status(400).send({ error: true, message: 'Please provide user_id/carrito_id' });

    } else {

        Reservation.createReservation(new_reservation, function(err, reservation) {

            if (err)
                res.send(err);
            res.json(reservation);
        });
    }
};


exports.read_a_reservation = function(req, res) {
    Reservation.getReservationById(req.params.reservationId, function(err, reservation) {
        if (err)
            res.send(err);
        res.json(reservation);
    });
};


exports.update_a_reservation = function(req, res) {
    Reservation.updateById(req.params.reservationId, new Reservation(req.body), function(err, reservation) {
        if (err)
            res.send(err);
        res.json(reservation);
    });
};


exports.delete_a_reservation = function(req, res) {
    Reservation.remove(req.params.reservationId, function(err, reservation) {
        if (err)
            res.send(err);
        res.json({ message: 'Reservation successfully deleted' });
    });
};