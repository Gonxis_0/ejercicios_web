'use strict';
let sql = require('./db.js');

//Reservation object constructor
let ReservationHasBooking = function(reservationHasBooking) {
    this.reservation_id = reservationHasBooking.reservation_id;
    this.reservation_user_id = reservationHasBooking.reservation_user_id;
    this.reservation_carrito_id = reservationHasBooking.reservation_carrito_id;
    this.booking_id = reservationHasBooking.booking_id;
    this.booking_user_id = reservationHasBooking.booking_user_id;
};
ReservationHasBooking.createReservation = function createReservation(newReservationHasBooking, result) {
    sql.query("INSERT INTO reservation_has_booking set ?", newReservationHasBooking, function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
ReservationHasBooking.getReservationHasBookingById = function createReservationHasBooking(reservationHasBookingId, result) {
    sql.query("Select reservation_id, reservation_user_id, reservation_carrito_id, booking_id, booking_user_id from reservation where id = ? ", reservationHasBookingId, function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, res);

        }
    });
};
ReservationHasBooking.getAllReservationHasBooking = function getAllReservationHasBooking(result) {
    sql.query("Select * from reservation_has_booking", function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('reservations_has_booking : ', res);

            result(null, res);
        }
    });
};
ReservationHasBooking.updateById = function(id, reservationHasBooking, result) {
    sql.query("UPDATE reservation_has_booking SET reservation_id = ?, reservation_user_id = ?, reservation_carrito_id = ?, booking_id = ?, booking_user_id = ? WHERE id = ?", [reservationHasBooking.reservation_id, reservationHasBooking.reservation_user_id, reservationHasBooking.reservation_carrito_id. reservationHasBooking.booking_id, reservationHasBooking.booking_user_id, id], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};
ReservationHasBooking.remove = function(id, result) {
    sql.query("DELETE FROM reservation_has_booking WHERE id = ?", [id], function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {

            result(null, res);
        }
    });
};

module.exports = ReservationHasBooking;