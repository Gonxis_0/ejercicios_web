'use strict';
let sql = require('./db.js');

//Trolley object constructor
let Trolley = function(trolley) {
    this.description = trolley.description;
    this.type = trolley.type;
    this.created = new Date();
    this.booking_id = trolley.booking_id;
    this.booking_user_id = trolley.booking_user_id;
};
Trolley.createTrolley = function createTrolley(newTrolley, result) {
    sql.query("INSERT INTO trolley set ?", newTrolley, function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Trolley.getTrolleyById = function createTrolley(trolleyId, result) {
    sql.query("Select * from trolley where id = ? ", trolleyId, function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, res);

        }
    });
};
Trolley.getAllTrolley = function getAllTrolley(result) {
    sql.query("Select * from trolley", function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('trolleys : ', res);

            result(null, res);
        }
    });
};
Trolley.updateById = function(id, trolley, result) {
    sql.query("UPDATE trolley SET description = ?, type = ?, booking_id = ?, booking_user_id = ? WHERE id = ?", [trolley.description, trolley.type, trolley.booking_id, trolley.booking_user_id, id], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};
Trolley.remove = function(id, result) {
    sql.query("DELETE FROM trolley WHERE id = ?", [id], function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {

            result(null, res);
        }
    });
};

module.exports = Trolley;