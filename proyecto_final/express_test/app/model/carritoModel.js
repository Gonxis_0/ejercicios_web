'use strict';
let sql = require('./db.js');

//Carrito object constructor
let Carrito = function(carrito) {
    this.title = carrito.title;
    this.description = carrito.description;
    this.type = carrito.type;
    this.created = new Date();
};
Carrito.createCarrito = function createCarrito(newCarrito, result) {
    sql.query("INSERT INTO carrito set ?", newCarrito, function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Carrito.getCarritoById = function createCarrito(carritoId, result) {
    sql.query("Select id, title, description, type, created from carrito where id = ? ", carritoId, function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, res);

        }
    });
};
Carrito.getAllCarrito = function getAllCarrito(result) {
    sql.query("Select * from carrito", function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('carritos : ', res);

            result(null, res);
        }
    });
};
Carrito.updateById = function(id, carrito, result) {
    sql.query("UPDATE carrito SET title = ?, description = ?, type = ? WHERE id = ?", [carrito.title, carrito.description, carrito.type, id], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};
Carrito.remove = function(id, result) {
    sql.query("DELETE FROM carrito WHERE id = ?", [id], function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {

            result(null, res);
        }
    });
};

module.exports = Carrito;