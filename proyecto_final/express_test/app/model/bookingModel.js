'use strict';
let sql = require('./db.js');

//Booking object constructor
let Booking = function(booking) {
    this.from_date = booking.from_date;
    this.until_date = booking.until_date;
    this.type = booking.type;
    this.created = new Date();
    this.user_id = booking.user_id;
};
Booking.createBooking = function createBooking(newBooking, result) {
    sql.query("INSERT INTO booking set ?", newBooking, function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Booking.getBookingById = function createBooking(bookingId, result) {
    sql.query("Select id, from_date, until_date, type, created, user_id from booking where id = ? ", bookingId, function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, res);

        }
    });
};
Booking.getAllBooking = function getAllBooking(result) {
    sql.query("Select * from booking", function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('bookings : ', res);

            result(null, res);
        }
    });
};
Booking.updateById = function(id, booking, result) {
    sql.query("UPDATE booking SET from_date = ?, until_date = ?, type = ? WHERE id = ?", [booking.from_date, booking.until_date, booking.type, id], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};
Booking.remove = function(id, result) {
    sql.query("DELETE FROM booking WHERE id = ?", [id], function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {

            result(null, res);
        }
    });
};

module.exports = Booking;