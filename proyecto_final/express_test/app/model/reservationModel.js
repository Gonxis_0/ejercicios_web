'use strict';
let sql = require('./db.js');

//Reservation object constructor
let Reservation = function(reservation) {
    this.user_id = reservation.user_id;
    this.carrito_id = reservation.carrito_id;
};
Reservation.createReservation = function createReservation(newReservation, result) {
    sql.query("INSERT INTO reservation set ?", newReservation, function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Reservation.getReservationById = function createReservation(reservationId, result) {
    sql.query("Select id, user_id, carrito_id from reservation where id = ? ", reservationId, function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, res);

        }
    });
};
Reservation.getAllReservation = function getAllReservation(result) {
    sql.query("Select * from reservation", function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('reservations : ', res);

            result(null, res);
        }
    });
};
Reservation.updateById = function(id, reservation, result) {
    sql.query("UPDATE reservation SET user_id = ?, carrito_id = ? WHERE id = ?", [reservation.user_id, reservation.carrito_id, id], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};
Reservation.remove = function(id, result) {
    sql.query("DELETE FROM reservation WHERE id = ?", [id], function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {

            result(null, res);
        }
    });
};

module.exports = Reservation;