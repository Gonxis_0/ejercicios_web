'user strict';
let sql = require('./db.js');

//User object constructor
let User = function(user) {
    this.name = user.name;
    this.surname = user.surname;
    this.email = user.email;
    this.genre = user.genre;
    this.isAdmin = user.isAdmin;
    this.created = new Date();
};
User.createUser = function createUser(newUser, result) {
    sql.query("INSERT INTO user set ?", newUser, function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
User.getUserById = function createUser(userId, result) {
    sql.query("Select id, name, surname, email, genre, created, isAdmin from user where id = ? ", userId, function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(err, null);
        } else {
            result(null, res);

        }
    });
};
User.getAllUser = function getAllUser(result) {
    sql.query("Select * from user", function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            console.log('users : ', res);

            result(null, res);
        }
    });
};
User.updateById = function(id, user, result) {
    sql.query("UPDATE user SET name = ?, surname = ?, email = ?, genre = ?, isAdmin = ? WHERE id = ?", [user.name, user.surname, user.email, user.genre, user.isAdmin, id], function(err, res) {
        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {
            result(null, res);
        }
    });
};
User.remove = function(id, result) {
    sql.query("DELETE FROM user WHERE id = ?", [id], function(err, res) {

        if (err) {
            console.log("error: ", err);
            result(null, err);
        } else {

            result(null, res);
        }
    });
};

module.exports = User;