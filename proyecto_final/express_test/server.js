/* let
    express = require('express'),
    app = express(),
    port = process.env.PORT || 3003;

app.listen(port);

console.log('TOMO 2 React Project RESTful API server started on: ' + port); */

const express = require('express'),
    app = express(),
    bodyParser = require('body-parser');
port = process.env.PORT || 3003;


const mysql = require('mysql');
// connection configurations
const mc = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Gonzalo12345',
    database: 'Tomo2_react_db'
});

// connect to database
mc.connect();

app.listen(port);

console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./app/routes/approutes'); //importing route
routes(app); //register the route