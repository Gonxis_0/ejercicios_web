//importamos/requerimos express y controladores
const express = require('express');
const usersRouter = require('./routes/user-controller');
const carritosRouter = require('./routes/carrito-controller');
const bookingsRouter = require('./routes/booking-controller');
const indexRouter = require('./routes/index-controller');

//instanciamos nueva aplicación express
const app = express();

//necesario para poder recibir datos en json
app.use(express.json());

//las ruta "/" se gestiona en indexRouter
app.use('/', indexRouter);

//las rutas que empiecen por /api/users se dirigirán a usersRouter
app.use('/api/users', usersRouter);

//las rutas que empiecen por /api/carritos se dirigirán a carritosRouter
app.use('/api/carritos', carritosRouter);

//las rutas que empiecen por /api/bookings se dirigirán a bookingsRouter
app.use('/api/bookings', bookingsRouter);

//arranque del servidor
const port = 3005
app.listen(port, () => console.log(`App listening on port ${port}!`))