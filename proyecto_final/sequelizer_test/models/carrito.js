module.exports = (sequelize, DataTypes) => {
    const Carrito = sequelize.define('Carrito', {
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        type: DataTypes.STRING
    }, {
        timestamps: false
    }, {
        tableName: 'carrito'
    });
    return Carrito;
};