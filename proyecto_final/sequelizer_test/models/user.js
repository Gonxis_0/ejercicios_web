module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        name: DataTypes.STRING,
        surname: DataTypes.STRING,
        email: DataTypes.STRING,
        genre: DataTypes.STRING,
        isAdmin: DataTypes.TINYINT
    }, {
        timestamps: false
    }, {
        tableName: 'user'
    });
    return User;
};