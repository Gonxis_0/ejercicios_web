module.exports = (sequelize, DataTypes) => {
    const Booking = sequelize.define('Booking', {
        from_date: DataTypes.DATE,
        until_date: DataTypes.DATE,
        type: DataTypes.STRING,
        created: DataTypes.DATE,
        user_id: DataTypes.INTEGER,
        carrito_id: DataTypes.INTEGER
    }, {
        timestamps: false
    }, {
        tableName: 'booking'
    });
    return Booking;
};