import React from 'react';

import './css/lista.css';

const Lista = (props) => {

    let items = props.palabras
        .map(item => <li key={item}>{item}</li>);

    return (
        <div className="lista">
        <ul>{items}</ul>
        </div>
    );
}

export default Lista;