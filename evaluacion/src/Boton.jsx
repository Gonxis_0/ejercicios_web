import React from 'react';

const Boton = (props) => {
    return <button onClick={props.onClick} >{props.textoBoton}</button>
}

export default Boton;