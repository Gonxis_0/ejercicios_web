import React from "react";

import Lista from './Lista';
import Palabra from './Palabra';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        palabra: '',
        palabras: [],
        add: "Add word",
        clear: "Clear all list"
    }

    this.saveWord = this.saveWord.bind(this);
    this.delWords = this.delWords.bind(this);
}

saveWord(datos){

  let nuevo = datos.palabra;
  let nuevaLista = this.state.palabras;
  console.log(nuevo);
  console.log(nuevaLista);

  nuevaLista.push(nuevo);

  this.setState({palabras: nuevaLista});

}

delWords() {
  this.setState({palabras: []});
}

  render() {
    return (
      <>
        <Palabra saveWord={this.saveWord} delWords={this.delWords}/>
        <Lista palabras={this.state.palabras}/>
      </>
    );
  }
}
