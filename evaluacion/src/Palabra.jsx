import React from 'react';

import './css/palabra.css'
import Boton from "./Boton";

 export default class Palabra extends React.Component {

     constructor(props) {
        super(props);
        this.state = {
            palabra: '',
            palabras: [],
            add: "Add word",
            clear: "Clear all list"
        }

        this.onChangeValue = this.onChangeValue.bind(this);
        this.onAddItem = this.onAddItem.bind(this);
    }

    onChangeValue = event => {
        this.setState({ palabra: event.target.value });
    };

    onAddItem = () => {
        this.props.saveWord ({
            palabra: this.state.palabra
        });

        this.setState(state => {
            const palabras = [...state.palabras, state.palabra];

            return {
                palabras,
                palabra: '',
            };
        });
    };

    render() {
        return (
            <div className="palabra">
                <input onChange={this.onChangeValue} name="palabra" value={this.state.palabra} />
                <Boton onClick={this.onAddItem} textoBoton={this.state.add} />
                <Boton onClick={this.props.delWords} textoBoton={this.state.clear} />
            </div>
        );
    }

}